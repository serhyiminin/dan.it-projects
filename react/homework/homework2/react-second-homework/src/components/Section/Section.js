import PropTypes from "prop-types";
import React from "react";
import styles from "./Section.module.scss";
import CardList from "../CardList/CardList";
import SectionTitle from "../SectionTitle/SectionTitle";

const Section = (props) => {
  const localProps = {
    title: "LATEST ARRIVALS IN MUSICA",
  };
  const { setIsEmpty, cartFlag } = props;

  return (
    <section className={styles.container}>
      <SectionTitle title={localProps.title} />
      <div className={styles.cards}>
        <CardList setIsEmpty={setIsEmpty} cartFlag={cartFlag} />
      </div>
    </section>
  );
};

Section.propTypes = {
  setIsEmpty: PropTypes.func,
};

export default Section;
