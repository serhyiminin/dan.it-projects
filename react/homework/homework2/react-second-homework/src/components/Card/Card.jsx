import React, { useState } from "react";
import PropTypes from "prop-types";
import cardStyle from "./Card.module.scss";
import Button from "../Button/Button";
import buttonStyles from "../Button/Button.module.scss";
import Modal from "../Modal/Modal";
import colors from "../Modal/Modal.module.scss";
import Icon from "@mdi/react";
import { mdiHexagram, mdiHexagramOutline } from "@mdi/js";

const Card = (props) => {
  const { id, name, price, image, isFavorite, toggleFav, addToCart } = props;

  const [isOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <div className={cardStyle.box}>
      <img src={image} alt={name} />
      <p>{name}</p>
      <div className={cardStyle.favorites} onClick={() => toggleFav(id)}>
        {!isFavorite && (
          <Icon path={mdiHexagramOutline} title="not Favorite" size={1} />
        )}
        {isFavorite && (
          <Icon path={mdiHexagram} title="Favorite" size={1} color="red" />
        )}
      </div>

      <div className={cardStyle.line}>
        <span className={cardStyle.price}>${price}</span>
        <Button
          className={`${buttonStyles.button} ${buttonStyles.landing} ${buttonStyles.black}`}
          text="ADD TO CART"
          onClick={openModal}
        />
      </div>

      {isOpen && (
        <Modal
          closeModal={closeModal}
          className={`${colors.violet}`}
          header="BUY AN ITEM"
          text="Do you want to add the item to the shopping cart?"
          actions={
            <>
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                text="Buy"
                onClick={() => addToCart(id)}
              />
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                text="Cancel"
                onClick={closeModal}
              />
            </>
          }
        />
      )}
    </div>
  );
};

Card.propTypes = {
  addToCart: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  image: PropTypes.string,
  isFavorite: PropTypes.bool,
  name: PropTypes.string,
  price: PropTypes.number,
  toggleFav: PropTypes.func.isRequired,
};

export default Card;
