import React from "react";
import PropTypes from "prop-types";
import styles from "./SectionTitle.module.scss";
import Icon from "@mdi/react";
import { mdiArrowRightBox, mdiArrowLeftBox } from "@mdi/js";

const SectionTitle = (props) => {
  const { title } = props;

  return (
    <div className={styles.wrapper}>
      <h1 className={styles.title}>{title}</h1>
      <div className={styles.arrows}>
        <Icon
          path={mdiArrowLeftBox}
          title="Go Left"
          size={"20px"}
          vertical
          color="#cc3333"
          cursor="pointer"
        />
        <Icon
          path={mdiArrowRightBox}
          title="Go Right"
          size={"20px"}
          vertical
          color="#cc3333"
          cursor="pointer"
        />
      </div>
    </div>
  );
};

SectionTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default SectionTitle;
