import PropTypes from "prop-types";
import React, { useState, useEffect } from "react";
import Card from "../Card/Card";

const CardList = (props) => {
  const { cartFlag } = props;
  const [cards, setCards] = useState([]);

  useEffect(() => {
    (async () => {
      const { data } = await fetch("http://localhost:3000/goods.json").then(
        (res) => res.json()
      );
      setCards(data);
    })();
  }, []);

  const toggleFav = (id) => {
    const index = cards.findIndex(({ id: arrayId }) => {
      return id === arrayId;
    });
    cards[index].isFavorite = !cards[index].isFavorite;
    setCards((current) => {
      const newState = [...current];
      return newState;
    });

    const storedCards = JSON.parse(localStorage.getItem("Favorites")) || [];
    const isFind = storedCards.findIndex(({ id: aId }) => {
      return id === aId;
    });

    if (isFind === -1) {
      const newItem = cards[index];
      storedCards.push(newItem);
      localStorage.setItem("Favorites", JSON.stringify(storedCards));
    } else {
      storedCards.splice(isFind, 1);
      localStorage.setItem("Favorites", JSON.stringify(storedCards));
      console.log("storedCards.length: ", storedCards.length);
      storedCards.length === 0 && localStorage.removeItem("Favorites");

      setCards((current) => {
        const newState = [...current];
        return newState;
      });
    }
  };

  const addToCart = (id) => {
    cartFlag(false);
    const ind = cards.findIndex(({ id: arrayId }) => {
      return id === arrayId;
    });
    const newCartItem = cards[ind];
    const oldCartItems = JSON.parse(localStorage.getItem("Cart")) || [];
    oldCartItems.push(newCartItem);
    localStorage.setItem("Cart", JSON.stringify(oldCartItems));
  };

  return (
    cards &&
    cards.map((item) => {
      return (
        <Card
          toggleFav={toggleFav}
          key={item.name}
          {...item}
          addToCart={addToCart}
        />
      );
    })
  );
};

CardList.propTypes = {
  setIsEmpty: PropTypes.func,
};

export default CardList;
