import React, { useState } from "react";
import Section from "./components/Section/Section";
import UpperHeader from "./components/UpperHeader/UpperHeader";
import styles from "./App.module.scss";

function App() {
  const [isEmpty, setIsEmpty] = useState(() => {
    let state = false;
    try {
      state = JSON.parse(window.localStorage.getItem("Cart")) || false;
      state ? (state = false) : (state = true);
    } catch (error) {
      state = false;
    }
    return state;
  });

  const cartFlag = (value) => {
    setIsEmpty(value);
  };

  return (
    <div className={styles.main}>
      <UpperHeader isEmpty={isEmpty} />
      <Section setIsEmpty={setIsEmpty} cartFlag={cartFlag} />
    </div>
  );
}

export default App;
