import React, { useState } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import cardStyle from "./Card.module.scss";
import Button from "../Button/Button";
import buttonStyles from "../Button/Button.module.scss";
import Modal from "../Modal/Modal";
import colors from "../Modal/Modal.module.scss";
import Icon from "@mdi/react";
import { mdiStarOutline, mdiStar } from "@mdi/js";
import {
  toggleIsFavoriteCards,
  updateCart,
  setIsOpenModal,
} from "../../store/actionCreators/actionCreator";

const Card = (props) => {
  let {
    id,
    name,
    price,
    image,
    isFavorite,
    btnText,
    btnColor,
    addOrDeleteInCart,
    modalHeader,
    question,
    btnModalTextConfirm,
    item,
    quantity,
  } = props;

  const dispatch = useDispatch();

  // const [isModalOpen, setIsModalOpen] = useState(false);
  const { isOpen } = useSelector((state) => state.modal);
  const closeModal = () => dispatch(setIsOpenModal(false));
  console.log("isOpen: ", isOpen);
  const [num, setNum] = useState(quantity);

  // const openModal = () => {
  //   setIsModalOpen(true);
  // };

  // const closeModal = () => {
  //   setIsModalOpen(false);
  // };

  return (
    <div className={cardStyle.box} id={id}>
      <img src={image} alt={name} />
      <p>{name}</p>
      <div className={cardStyle.wrapper}>
        <div
          className={cardStyle.favorites}
          onClick={() => dispatch(toggleIsFavoriteCards(id, item))}
        >
          {!isFavorite && (
            <Icon path={mdiStarOutline} title="not Favorite" size={1} />
          )}
          {isFavorite && (
            <Icon path={mdiStar} title="Favorite" size={1} color="red" />
          )}
        </div>
        {quantity && (
          <input
            className={cardStyle.quantity}
            type="number"
            min={1}
            max={10000000}
            value={num}
            onChange={(e) => {
              setNum(e.target.value);
              item = { ...item, quantity: e.target.value };
              dispatch(updateCart(item));
            }}
          />
        )}
      </div>
      <div className={cardStyle.line}>
        <span className={cardStyle.price}>${price}</span>
        <Button
          className={`${buttonStyles.button} ${buttonStyles.landing} ${btnColor}`}
          btnText={btnText}
          onClick={() => {
            dispatch(setIsOpenModal(true));
          }}
        />
      </div>

      {isOpen && (
        <Modal
          // item={item}
          // closeModal={closeModal}
          className={`${colors.violet}`}
          modalHeader={modalHeader}
          question={question}
          actions={
            <>
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                btnText={btnModalTextConfirm}
                onClick={() => {
                  addOrDeleteInCart(item);
                }}
              />
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                btnText="Cancel"
                onClick={closeModal}
              />
            </>
          }
        />
      )}
    </div>
  );
};

export default Card;
