import PropTypes from "prop-types";
import React from "react";
import Section from "../../components/Section/Section";

const FavoritePage = (props) => {
  const localProps = {
    title: "FAVORITE GOODS",
  };

  return <Section title={localProps.title} />;
};

export default FavoritePage;
