import { SET_IS_OPEN_MODAL, SET_CONFIG_MODAL } from "../actions/actions";

const initialValues = {
  isOpen: false,
  id: "",
  title: "",
};

const modalReducer = (state = initialValues, { type, payload }) => {
  switch (type) {
    case SET_IS_OPEN_MODAL: {
      return { ...state, isOpen: payload };
    }
    case SET_CONFIG_MODAL: {
      return { ...state, title: payload?.title, id: payload?.id };
    }

    default:
      return state;
  }
};

export default modalReducer;
