import { combineReducers } from "redux";
import cardReducer from "./cardReducer";
import cartReducer from "./cartReducer";
import modalReducer from "./modalReducer";
// import forceUpdateReducer from "./forceUpdateReducer";

const reducer = combineReducers({
  card: cardReducer,
  cart: cartReducer,
  modal: modalReducer,
  // forceUpdater: forceUpdateReducer,
});
export default reducer;
