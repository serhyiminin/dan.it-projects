import {
  INIT_CARDS,
  SET_IS_LOADING_CARDS,
  TOGGLE_IS_FAVORITE_CARDS,
} from "../actions/actions";

const initialState = {
  data: [],
  isLoading: true,
};

const cardReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case INIT_CARDS: {
      return { ...state, data: payload };
    }
    case SET_IS_LOADING_CARDS: {
      return { ...state, isLoading: payload };
    }
    case TOGGLE_IS_FAVORITE_CARDS:
      const newCards = [...state.data];
      const index = newCards.findIndex((elem) => elem.id === payload.id);
      if (index !== -1) {
        newCards[index] = payload;
      }
      return { ...state, data: newCards };
    default: {
      return state;
    }
  }
};

export default cardReducer;
