import React from "react";
import styles from "./Header.module.scss";
import HeaderNav from "../HeaderNav/HeaderNav";

const Header = () => {
  return (
    <div className={styles.wrap}>
      <HeaderNav />
    </div>
  );
};

export default Header;
