import React from "react";
import Section from "../../components/Section/Section";

const HomePage = (props) => {
  const localProps = {
    title: "LATEST ARRIVALS IN MUSICA",
  };

  return <Section title={localProps.title} />;
};

export default HomePage;
