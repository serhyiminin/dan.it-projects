import React, { memo } from "react";
import PropTypes from "prop-types";
import style from "./CustomInputForField.module.scss";

const CustomInputForField = (props) => {
  const { type, label, field, form } = props;

  const touched = form.touched[field.name];
  const error = form.errors[field.name];

  return (
    <>
      <label>{label}</label>
      <input type={type} value={field.value} {...field} />
      <span className={style.error}>{touched && error ? error : ""}</span>
    </>
  );
};

CustomInputForField.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  }),
  form: PropTypes.shape({
    errors: PropTypes.object,
    touched: PropTypes.object,
  }),
  label: PropTypes.string,
  type: PropTypes.string,
};
CustomInputForField.defaultProps = {
  type: "text",
  label: "",
};

export default memo(CustomInputForField);
