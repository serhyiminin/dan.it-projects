import React, { memo } from "react";
import styles from "./ButtonOrder.module.scss";
import Button from "@mui/material/Button";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { setIsOpenCheckout } from "../../store/actionCreators/actionCreator";

const ButtonOrder = () => {
  const dispatch = useDispatch();
  const { data: cart } = useSelector((store) => store.cart, shallowEqual);

  return (
    <section className={styles.container}>
      <Button
        disabled={cart.length < 0}
        variant="contained"
        className={styles.Button}
        color="error"
        onClick={() => dispatch(setIsOpenCheckout(true))}>
        Place an Order
      </Button>
    </section>
  );
};

export default memo(ButtonOrder);
