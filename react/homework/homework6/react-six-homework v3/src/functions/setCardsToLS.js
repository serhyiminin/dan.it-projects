function setCardsToLS(keyName, array) {
  localStorage.setItem(keyName, JSON.stringify(array));
}
export default setCardsToLS;
