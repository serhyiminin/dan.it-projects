import React from "react";
import ButtonOrder from "./ButtonOrder";
import { render } from "@testing-library/react";
import { Provider, useDispatch } from "react-redux";
import { setIsOpenCheckout } from "../../store/actionCreators/actionCreator";
import store from "../../store/index";

const handleClick = jest.fn();

const PlaceButton = () => {
  const dispatch = useDispatch();

  return (
    <button onClick={() => dispatch(setIsOpenCheckout(true))}>
      Place an order
    </button>
  );
};
const Component = () => {
  return (
    <Provider store={store}>
      <PlaceButton />
      <ButtonOrder />
    </Provider>
  );
};

describe("ButtonOrder render test", () => {
  test("should button order render", () => {
    const { asFragment } = render(<Component />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe("Place an order is open", () => {
  test("should button order click", () => {
    const { getByText } = render(<Component onClick={handleClick()} />);
    getByText("Place an order").click();
    expect(handleClick).toBeCalled();
  });
});
