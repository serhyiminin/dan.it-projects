import React, { memo } from "react";
import { shallowEqual, useSelector } from "react-redux";
import PropTypes from "prop-types";
import styles from "./Section.module.scss";
import CardList from "../CardList/CardList";
import SectionTitle from "../SectionTitle/SectionTitle";
import ButtonOrder from "../ButtonOrder/ButtonOrder";
import Form from "../Form/Form";

const Section = (props) => {
  const { title } = props;
  const { isCheckout } = useSelector((state) => state.checkout, shallowEqual);

  return (
    <section>
      {title === "CART" && <ButtonOrder />}
      <div className={styles.container}>
        <SectionTitle title={title} />
        <div className={styles.cards}>
          <CardList title={title} />
        </div>
      </div>
      {isCheckout && <Form />}
    </section>
  );
};

Section.propTypes = {
  title: PropTypes.string.isRequired,
};

Section.defaultProps = {
  title: "CART",
};
export default memo(Section);
