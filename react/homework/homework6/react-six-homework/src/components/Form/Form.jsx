import React, { useEffect, memo } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { Formik, Form as FormikForm, FastField } from "formik";
import * as yup from "yup";
import CustomInputForField from "../CustomInputForField";
import CustomInputForPhone from "../CustomInputForPhone";
import styles from "./Form.module.scss";
import {
  doCheckout,
  setIsOpenCheckout,
} from "../../store/actionCreators/actionCreator";

const Form = () => {
  const dispatch = useDispatch();
  const { data: cart } = useSelector((store) => store.cart, shallowEqual);

  const escFunction = (event) => {
    if (event.keyCode === 27) {
      dispatch(setIsOpenCheckout(false));
    }
  };

  useEffect(() => {
    document.addEventListener("keydown", escFunction);
    return () => {
      document.removeEventListener("keydown", escFunction);
    };
  }, []);

  const initialValues = {
    name: "",
    lastName: "",
    age: "",
    address: "",
    phone: "",
  };

  const validationSchema = yup.object().shape({
    name: yup.string().required("Required field"),
    lastName: yup.string().required("Required field"),
    age: yup
      .number()
      .typeError("You must specify you age as a number")
      .min(18, "You are too young, ask your parents")
      .max(110, "Send us a letter ang take the goods as a gift!")
      .required("Required field"),
    address: yup.string().required("Required field"),
    phone: yup.string().required("Required field"),
  });

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values) => {
        console.log("Users data", values);
        cart.map((item) => {
          console.log("Purchased item", item);
        });
        dispatch(doCheckout());
        dispatch(setIsOpenCheckout(false));
      }}
      validationSchema={validationSchema}>
      <FormikForm>
        <FastField
          name="name"
          label="First Name"
          component={CustomInputForField}
        />
        <FastField
          name="lastName"
          label="Last Name"
          component={CustomInputForField}
        />
        <FastField name="age" label="Age" component={CustomInputForField} />
        <FastField
          name="address"
          label="Address"
          component={CustomInputForField}
        />
        <FastField name="phone" label="Phone" component={CustomInputForPhone} />

        <button className={styles.btn_checkout} type="submit">
          Checkout
        </button>
      </FormikForm>
    </Formik>
  );
};

Form.propTypes = {};
Form.defaultProps = {};

export default memo(Form);
