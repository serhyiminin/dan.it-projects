function getCardsFromLS(keyName) {
  try {
    let state = JSON.parse(localStorage.getItem(keyName)) || [];
    return state;
  } catch (error) {
    console.log("getCardsFromLS: ", error);
  }
}

export default getCardsFromLS;
