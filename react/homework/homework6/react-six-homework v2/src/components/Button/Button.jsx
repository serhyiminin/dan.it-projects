import PropTypes from "prop-types";
import React, { memo } from "react";

const Button = (props) => {
  const { btnText, onClick, className, bType } = props;
  return (
    <button type={bType && "button"} className={className} onClick={onClick}>
      {btnText}
    </button>
  );
};

Button.propTypes = {
  bType: PropTypes.string,
  btnText: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  btnText: "Modal Button  by default",
  className: "active by default",
  onClick: () => {
    console.log("Click on Modal Button by default");
  },
};

export default memo(Button);
