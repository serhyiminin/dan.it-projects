//Cards
export const INIT_CARDS = "GET_CARDS";
export const TOGGLE_IS_FAVORITE_CARDS = "TOGGLE_IS_FAVORITE_CARDS";

//Cart
export const INIT_FROM_CARTS = "INIT_FROM_CARTS";
export const ADD_TO_CART = "ADD_TO_CART";
export const UPDATE_CART = "UPDATE_CART";
export const DELETE_FROM_CART = "DELETE_FROM_CART";
export const DELETE_CART = "DELETE_CART";

//Header
export const SET_IS_LOADING_CARDS = "SET_IS_LOADING_CARDS";

//Modal
export const SET_IS_OPEN_MODAL = "SET_IS_OPEN_MODAL";
export const SET_CONFIG_MODAL = "SET_CONFIG_MODAL";

//FormCheckout
export const SET_IS_OPEN_CHECKOUT = "SET_IS_OPEN_CHECKOUT";
