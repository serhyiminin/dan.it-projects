import React from "react";
import { Switch, Route } from "react-router-dom";
import HomePage from "../pages/HomePage/HomePage";
import CartPage from "../pages/CartPage/CartPage";
import FavoritePage from "../pages/FavoritePage/FavoritePage";

function Routes() {
  return (
    <Switch>
      <Route exact path="/" render={() => <HomePage title="HomePage" />} />
      <Route
        exact
        path="/favorite"
        render={() => <FavoritePage title={`FavPage`} />}
      />
      <Route
        exact
        path="/cart"
        render={() => <CartPage title={`CartPage`} />}
      />
    </Switch>
  );
}

export default Routes;
