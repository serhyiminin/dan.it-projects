import PropTypes from "prop-types";
import React, { memo } from "react";

const Button = (props) => {
  const { btnText, onClick, className } = props;
  return (
    <button className={className} onClick={onClick}>
      {btnText}
    </button>
  );
};

Button.propTypes = {
  btnText: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default memo(Button);
