import PropTypes from "prop-types";
import React from "react";
import { Switch, Route } from "react-router-dom";
import HomePage from "../pages/HomePage/HomePage";
import CartPage from "../pages/CartPage/CartPage";
import FavoritePage from "../pages/FavoritePage/FavoritePage";

function Routes(props) {
  const { cards, addToCart, toggleFav, storedCards, deleteFromCart } = props;

  return (
    <Switch>
      <Route
        exact
        path="/"
        render={() => (
          <HomePage
            title="HomePage"
            cards={cards}
            addToCart={addToCart}
            toggleFav={toggleFav}
          />
        )}
      />
      <Route
        exact
        path="/favorite"
        render={() => (
          <FavoritePage
            cards={cards}
            title={`FavPage`}
            toggleFav={toggleFav}
            addToCart={addToCart}
          />
        )}
      />
      <Route
        exact
        path="/cart"
        render={() => (
          <CartPage
            title={`CartPage`}
            storedCards={storedCards}
            toggleFav={toggleFav}
            addToCart={addToCart}
            deleteFromCart={deleteFromCart}
          />
        )}
      />
    </Switch>
  );
}

Routes.propTypes = {
  addToCart: PropTypes.func.isRequired,
  cards: PropTypes.array.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  storedCards: PropTypes.array,
  toggleFav: PropTypes.func.isRequired,
};

export default Routes;
