import PropTypes from "prop-types";
import React from "react";

const Button = (props) => {
  const { btnText, onClick, className } = props;
  return (
    <button className={className} onClick={onClick}>
      {btnText}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  btnText: PropTypes.string.isRequired,
};

export default Button;
