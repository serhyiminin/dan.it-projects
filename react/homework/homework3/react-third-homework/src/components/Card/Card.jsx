import React, { useState } from "react";
import PropTypes from "prop-types";
import cardStyle from "./Card.module.scss";
import Button from "../Button/Button";
import buttonStyles from "../Button/Button.module.scss";
import Modal from "../Modal/Modal";
import colors from "../Modal/Modal.module.scss";
import Icon from "@mdi/react";
import { mdiStarOutline, mdiStar } from "@mdi/js";

const Card = (props) => {
  const {
    id,
    name,
    price,
    image,
    isFavorite,
    toggleFav,
    btnText,
    btnColor,
    addOrDeleteInCart,
    modalHeader,
    question,
    btnModalTextConfirm,
  } = props;

  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={cardStyle.box} id={id}>
      <img src={image} alt={name} />
      <p>{name}</p>
      <div className={cardStyle.favorites} onClick={() => toggleFav(id)}>
        {!isFavorite && (
          <Icon path={mdiStarOutline} title="not Favorite" size={1} />
        )}
        {isFavorite && (
          <Icon path={mdiStar} title="Favorite" size={1} color="red" />
        )}
      </div>

      <div className={cardStyle.line}>
        <span className={cardStyle.price}>${price}</span>
        <Button
          className={`${buttonStyles.button} ${buttonStyles.landing} ${btnColor}`}
          btnText={btnText}
          onClick={openModal}
        />
      </div>

      {isModalOpen && (
        <Modal
          closeModal={closeModal}
          className={`${colors.violet}`}
          modalHeader={modalHeader}
          question={question}
          actions={
            <>
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                btnText={btnModalTextConfirm}
                onClick={() => addOrDeleteInCart(id, closeModal)}
              />
              <Button
                className={`${buttonStyles.button} ${buttonStyles.modal} ${buttonStyles.violet}`}
                btnText="Cancel"
                onClick={closeModal}
              />
            </>
          }
        />
      )}
    </div>
  );
};

Card.propTypes = {
  addOrDeleteInCart: PropTypes.func.isRequired,
  btnColor: PropTypes.string,
  btnModalTextConfirm: PropTypes.string,
  btnText: PropTypes.string,
  id: PropTypes.number.isRequired,
  image: PropTypes.string,
  isFavorite: PropTypes.bool,
  modalHeader: PropTypes.string,
  name: PropTypes.string.isRequired,
  price: PropTypes.number,
  question: PropTypes.string.isRequired,
  toggleFav: PropTypes.func.isRequired,
};

export default Card;
