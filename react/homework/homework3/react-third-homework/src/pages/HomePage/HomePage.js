import PropTypes from "prop-types"
import React from "react";
import Section from "../../components/Section/Section";

const HomePage = (props) => {
  const { cards, addToCart, toggleFav } = props;

  const localProps = {
    title: "LATEST ARRIVALS IN MUSICA",
  };

  return (
    <Section
      title={localProps.title}
      addToCart={addToCart}
      toggleFav={toggleFav}
      cards={cards}
    />
  );
};

HomePage.propTypes = {
  addToCart: PropTypes.any,
  cards: PropTypes.any,
  toggleFav: PropTypes.any
}

export default HomePage;
