import PropTypes from "prop-types";
import React from "react";
import Section from "../../components/Section/Section";

const FavoritePage = (props) => {
  const { cards, toggleFav, addToCart } = props;
  const localProps = {
    title: "FAVORITE GOODS",
  };

  return (
    <Section
      title={localProps.title}
      addToCart={addToCart}
      toggleFav={toggleFav}
      cards={cards}
    />
  );
};

FavoritePage.propTypes = {
  addToCart: PropTypes.func.isRequired,
  cards: PropTypes.array.isRequired,
  toggleFav: PropTypes.func.isRequired,
};

export default FavoritePage;
