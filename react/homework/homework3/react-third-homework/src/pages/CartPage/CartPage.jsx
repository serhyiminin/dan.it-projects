import PropTypes from "prop-types";
import React from "react";
import Section from "../../components/Section/Section";

const CartPage = (props) => {
  const { storedCards, addToCart, toggleFav, deleteFromCart } = props;
  const localProps = {
    title: "CART",
  };

  return (
    <Section
      title={localProps.title}
      addToCart={addToCart}
      toggleFav={toggleFav}
      storedCards={storedCards}
      deleteFromCart={deleteFromCart}
    />
  );
};

CartPage.propTypes = {
  addToCart: PropTypes.func.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  toggleFav: PropTypes.func.isRequired,
  storedCards: PropTypes.array.isRequired,
};

export default CartPage;
