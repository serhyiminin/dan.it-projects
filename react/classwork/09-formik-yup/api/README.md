# TODO API

#### URL: http://localhost:3001/
<hr>

## POST /sign-up

#### Body:
```js
{
    name: '',
    age: '',
    city: '',
    email: '',
    password: '',
    repeatPassword: ''
}
```

#### Response: 
```js
{
    status: 'success',
    data: {
        user: {
            name: '',
            age: '',
            city: '',
            email: '',
            id: '',
        },
        token: ''
    }
}
```
<hr>

## POST /sign-in

#### Body:
```js
{
    email: '',
    password: '',
}
```

#### Responses:

status: 200
```js
{
    status: 'success',
    data: {
        user: {
            name: '',
            age: '',
            city: '',
            email: '',
            id: '',
        },
    }
}
```
<hr>


## POST /edit-profile
#### Headers:
`"Authorization": "Bearer HGHGhkgakfgkgJGj123j12j3gjkGJGj12g3j1g2j3gJGJGSjg1j23gj12hg3j1h23"`

#### Body:
```js
{
    nsme: '',
    age: '',
    city: '',
    id: '',
}
```

#### Responses:

status: 200
```js
{
    status: 'success',
    data: {
        user: {
            name: '',
            age: '',
            city: '',
            email: '',
            id: '',
        },
    }
}
```
<hr>