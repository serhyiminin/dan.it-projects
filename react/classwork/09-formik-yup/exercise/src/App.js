import React, {useEffect} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Header from "./components/Header/Header";
import './App.scss';
import Routes from "./Routes";

const App = () => {
    useEffect(() => {
        (async () => {
            const res = await fetch('http://localhost:3001/sign-in', {
                method: 'POST',
                body: JSON.stringify({
                    email: 'ffff@aaaa.com',
                    password: '123',
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json());

            console.log(res)
        })()
    }, [])
    return (
        <Router>
            <div className="App">
                <Header />
                <section>
                    <Routes />
                </section>
            </div>
        </Router>
    );
}

export default App;
