import React from 'react';
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";


const Header = () => {

    return (
        <header className={styles.root}>
            <span />
            <NavLink exact to="/" className={styles.homeLink} activeClassName={styles.activeLink}>Home</NavLink>

            <nav>
                <ul>
                    <li>
                        <NavLink exact to="/edit" activeClassName={styles.activeLink}>Edit Profile</NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/sign-in" activeClassName={styles.activeLink}>Sign In</NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/sign-up" activeClassName={styles.activeLink}>Sign Up</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
};


export default Header;
