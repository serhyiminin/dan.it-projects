import React, { useRef } from 'react';

const FormWithRef = () => {
    const nameRef = useRef(null);
    const emailRef = useRef(null);
    const passwordRef = useRef(null);

    const handleSubmit = (e) => {
        e.preventDefault();

        const body = {
            name: nameRef.current.value,
            email: emailRef.current.value,
            password: passwordRef.current.value,
        }

        console.log(body);

    }

    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <h3>REF</h3>
            <input
                type="text"
                name="name"
                placeholder="Name"
                ref={nameRef}
            />
            <span className="error"></span>

            <input
                type="text"
                name="email"
                placeholder="Email"
                ref={emailRef}
            />

            <input
                type="password"
                name="password"
                placeholder="Password"
                ref={passwordRef}
            />

            <button type="submit">Submit</button>
        </form>
    )
}

export default FormWithRef;