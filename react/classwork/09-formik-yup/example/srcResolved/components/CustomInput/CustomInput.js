import React, {useMemo} from 'react';
import PropTypes from 'prop-types';
import {useField} from "formik";

const CustomInput = (props) => {
    const { type, placeholder, label, field, form } = props;
    const { name, value } = field;
    const { touched, errors } = form;

    return useMemo(() => (
        <>
            {label && <label htmlFor={name}>{label}</label>}
            <input
                name={name}
                id={name}
                type={type}
                placeholder={placeholder}
                value={value}
                {...field}
            />
            <span className="error">{touched[name] && errors[name] ? errors[name] : ''}</span>
        </>
    ), [value, touched, errors])
}

CustomInput.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
};
CustomInput.defaultProps = {
    type: 'text',
    placeholder: '',
    label: '',
};

export default CustomInput;