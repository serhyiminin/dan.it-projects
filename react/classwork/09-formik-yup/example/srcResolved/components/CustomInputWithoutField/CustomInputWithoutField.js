import React from 'react';
import PropTypes from 'prop-types';
import CustomInput from "../CustomInput";
import {useField} from "formik";

const CustomInputWithoutField = (props) => {
    const { type, placeholder, label, name} = props;
    const [field, meta, helpers] = useField(props);

    return (
        <>
            <>
                {label && <label htmlFor={name}>{label}</label>}
                <input
                    name={name}
                    id={name}
                    type={type}
                    placeholder={placeholder}
                    {...field}
                />
                <span className="error">{meta.touched && meta.error ? meta.error : ''}</span>
            </>
        </>
    )
}

CustomInput.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
};
CustomInput.defaultProps = {
    type: 'text',
    placeholder: '',
    label: '',
};

export default CustomInputWithoutField;