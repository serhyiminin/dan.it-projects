import React from 'react';
import PropTypes from 'prop-types';

const CustomInput = (props) => {
    const { type, placeholder, } = props;

    return (
        <>
            <input
                type={type}
                placeholder={placeholder}
            />
        </>
    );
}

CustomInput.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
};
CustomInput.defaultProps = {
    type: 'text',
    placeholder: '',
};

export default CustomInput;