import React from 'react';


const FormWithFormik = () => {

    return (
        <form>
            <h3>FORMIK</h3>
            <input
                type="text"
                name="name"
                placeholder="Name"
            />

            <input
                type="text"
                name="email"
                placeholder="Email"
            />

            <input
                type="password"
                name="password"
                placeholder="Password"
            />

            <button type="submit">Submit</button>
        </form>
    )
}

export default FormWithFormik;