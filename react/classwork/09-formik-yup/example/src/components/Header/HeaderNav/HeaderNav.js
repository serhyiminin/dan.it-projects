import React from "react";
import styles from "./HeaderNav.module.scss"
import {NavLink} from "react-router-dom";


const HeaderNav = () => {
    return(
        <nav>
            <ul>
                <li>
                    <NavLink to="/ref" activeClassName={styles.activeLink}>Ref</NavLink>
                </li>
                <li>
                    <NavLink to="/hooks" activeClassName={styles.activeLink}>Hooks</NavLink>
                </li>
                <li>
                    <NavLink to="/formik" activeClassName={styles.activeLink}>Formik</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default HeaderNav