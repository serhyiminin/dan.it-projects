import React, {useEffect} from 'react';
import styles from './CartContainer.module.scss';
import CartItem from "../CartItem";
import {useDispatch, useSelector, connect} from "react-redux";
import {initItemsCart} from "../../../store/actionCreators";

const CartContainer = () => {
    const cartItems = useSelector(state => state.cart.data);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(initItemsCart());
    }, [])


    return (
        <div className={styles.cardContainer}>
            {cartItems && cartItems.map(cartItem => <CartItem key={cartItem.id} {...cartItem} />)}
        </div>
    )
}

export default CartContainer;