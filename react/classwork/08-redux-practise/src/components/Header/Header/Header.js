import React from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import {Link} from "react-router-dom";
import HeaderNav from "../HeaderNav/HeaderNav";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import { headerForceUpdate } from "../../../store/actionCreators";

const Header = () => {
    const isAuth = localStorage.getItem('authToken');
    const forceUpdates = useSelector(state => state.forceUpdater.headerForceUpdater, shallowEqual);
    const dispatch = useDispatch();
    console.log('headerForceUpdate', headerForceUpdate)

    return (
        <header className={styles.root}>
            <span>REDUX CARDS</span>
            <HeaderNav/>
            { !isAuth && <Link to='sign-in'>Sign In</Link> }
            { isAuth && <a onClick={() => {
                localStorage.removeItem('authToken');
                dispatch(headerForceUpdate())
            }}>Log Out</a>}
        </header>
    );
};

Header.propTypes = {
    title: PropTypes.string ,
    user: PropTypes.shape(
        {
            name: PropTypes.string,
            age: PropTypes.number,
            avatar: PropTypes.string
        }
    )
}

Header.defaultProps = {
    title: "Hello",
    user: {
        name:"Marina",
        age: 25,
        avatar: ""
    }
}

export default Header;
