import React, {useState} from 'react';
import styles from './CreateCardForm.module.scss';
import {Button, TextField} from "@mui/material";
import instance from "../../../api";

const CreateCardForm = () => {
    const [title, setTitle] = useState('');
    const [imageUrl, setImageUrl] = useState('');
    const [caption, setCaption] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        const body = {
            title,
            imageUrl,
            caption,
            isFavorite: false,
        }

        const result = await instance.post('', body)
        console.log(result)
    }

    const inputParams = {
        title: {
            type: 'text',
            name: 'title',
            label: 'Title',
            className: styles.input,
            value: title,
            onChange: ({target}) => setTitle(target.value),
        },
        imageUrl: {
            type: 'text',
            name: 'imageUrl',
            label: 'Image URL',
            className: styles.input,
            value: imageUrl,
            onChange: ({target}) => setImageUrl(target.value),
        },
        caption: {
            type: 'text',
            name: 'caption',
            label: 'Caption',
            className: styles.input,
            value: caption,
            onChange: ({target}) => setCaption(target.value),
        }
    };

    return (
        <div className={styles.root}>
            <form onSubmit={handleSubmit}>
                <div className={styles.inputWrapper}>
                    <TextField {...inputParams.title} />
                    <TextField {...inputParams.imageUrl} />
                    <TextField {...inputParams.caption} />
                </div>

                <Button type='submit' variant='contained'>Create card</Button>
            </form>
        </div>
    )
}

export default CreateCardForm;