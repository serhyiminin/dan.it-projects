import React, {useState, useEffect} from 'react';
import styles from './CardContainer.module.scss';
import instance from "../../../api";
import Card from "../Card";
import {Skeleton} from "@mui/material";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {INIT_CARDS} from "../../../store/actions";
import {initCards} from "../../../store/actionCreators";

const CardContainer = ({setCartItems, setNotificationConfig}) => {
    const {data: cards, isLoading} = useSelector((store) => store.card, shallowEqual);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(initCards())
    }, [])

    if (isLoading) {
        return (
            <div className={styles.cardContainer}>
                <Skeleton variant='rectangular' width={300} height={362} sx={{ margin: '10px', borderRadius: '8px' }} />
                <Skeleton variant='rectangular' width={300} height={362} sx={{ margin: '10px', borderRadius: '8px' }} />
                <Skeleton variant='rectangular' width={300} height={362} sx={{ margin: '10px', borderRadius: '8px' }} />
            </div>
        )
    }


    return (
        <div className={styles.cardContainer}>
            {cards.length ? cards.map(card => <Card key={card.id} {...card} setCartItems={setCartItems} setNotificationConfig={setNotificationConfig}/>) :
                <h2>No cards</h2>}
        </div>
    )
}

CardContainer.propTypes = {};
CardContainer.defaultProps = {};

export default CardContainer;