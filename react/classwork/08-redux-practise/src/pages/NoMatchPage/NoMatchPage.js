import React from 'react';
import NoMatch from "../../components/NoMatch";

const NoMatchPage = () => (
    <div>
       <NoMatch />
    </div>
);


export default NoMatchPage;