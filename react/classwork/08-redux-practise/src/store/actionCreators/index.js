import instance from "../../api";
import {
    ADD_ITEM_CART,
    HEADER_FORCE_UPDATE,
    INIT_CARDS,
    INIT_ITEM_CART,
    SET_IS_LOADING_CARDS,
    TOGGLE_IS_FAVOURITE_CARDS
} from "../actions";


export const initCards = () => async (dispatch) => {
    dispatch(setIsLoadingCards(true))
    const {status, data} = await instance.get('');
    if (status === 200) {
        dispatch({type: INIT_CARDS, payload: data})
    }
    dispatch(setIsLoadingCards(false))
}

export const toggleIsFavourite = (id, body) => async (dispatch) => {

    const {status, data} = await instance.put(`/${id}`, body);
    if (status === 200) {
        dispatch({type: TOGGLE_IS_FAVOURITE_CARDS, payload: data});
    }
}


export const setIsLoadingCards = (isLoading) => ({type: SET_IS_LOADING_CARDS, payload: isLoading})


//CART ----------------------------------------------------------------------------------

export const addItemCart = (item) => ({ type: ADD_ITEM_CART, payload: item });

export const initItemsCart = () => (dispatch) => {
    const cartLS = localStorage.getItem('cart');
    if (cartLS) {
        dispatch({ type: INIT_ITEM_CART, payload: JSON.parse(cartLS)})
    }
}

//FORCE UPDATER ----------------------------------------------------------------------------------

export const headerForceUpdate = () => ({ type: HEADER_FORCE_UPDATE });
