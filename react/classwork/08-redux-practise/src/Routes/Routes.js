import React from "react";
import {Switch, Route, Redirect} from 'react-router-dom'
import CardsPage from "../pages/CardsPage";
import NoMatchPage from "../pages/NoMatchPage";
import SignPage from "../pages/SignPage";
import CartPage from "../pages/CartPage";

const Routes = ({setCartItems, cartItems, setNotificationConfig}) => (
    <Switch>
        <Route exact path="/"><Redirect to='/cards'/></Route>
        <Route exact path="/cards">
            <CardsPage setCartItems={setCartItems} setNotificationConfig={setNotificationConfig}/>
        </Route>
        <Route exact path="/cart">
            <CartPage cartItems={cartItems}/>
        </Route>
        <Route exact path="/sign-in" component={SignPage}/>
        <Route path="*" component={NoMatchPage}/>
    </Switch>
);


export default Routes