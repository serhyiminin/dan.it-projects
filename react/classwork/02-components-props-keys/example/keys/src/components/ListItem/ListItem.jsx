import React, { PureComponent } from 'react';

class ListItem extends PureComponent {
    render(){
        console.log('RENDER: ', this.props.children);

        return(
            <li>{this.props.children}</li>
        )
    }
}

export default ListItem;
