import React from 'react';
import styles from './Item.module.scss';

const Item = (props) => {
    const { name, price, image } = props;

    return (
        <div className={styles.root}>
            <div className={styles.favourites}>
                Star
            </div>
            <p>{ name }</p>
            <img src={image} alt={name} />
            <span>{price}$</span>
            <button onClick={() => {}}>Add to cart</button>
        </div>
    )
}

export default Item;