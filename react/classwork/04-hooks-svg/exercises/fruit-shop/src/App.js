import React, {useEffect, useState} from "react";
import ItemsContainer from "./components/ItemsContainer";
import Cart from "./components/Cart";
import styles from './App.module.scss';
const items = [
    {
        name: 'Orange',
        price: 300,
        image: 'https://cdn.shopify.com/s/files/1/0409/2562/6532/products/online-gulayan-makati-orange-1pc-17411997663396_grande.jpg?v=1594059033',
    },
    {
        name: 'Lime',
        price: 500,
        image: 'https://static.libertyprim.com/files/familles/lime-large.jpg?1569491474',
    },
    {
        name: 'Lemon',
        price: 400,
        image: 'https://www.collinsdictionary.com/images/full/lemon_234304936.jpg',
    }
];


function App() {

  return (
    <div className={styles.root}>
      <div>
        <ItemsContainer items={items} />
      </div>

      <div>
        <Cart />
      </div>
    </div>
  );
}

export default App;
