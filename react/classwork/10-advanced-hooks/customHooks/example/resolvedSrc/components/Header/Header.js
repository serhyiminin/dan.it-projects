import React from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import useScrollPosition from "../../hooks/useScrollPosition";
import classNames from "classnames";

const Header= () => {
    const scrollY = useScrollPosition(10);

        return (
              <header className={classNames(styles.root, { [styles.scrolled]: scrollY > 100 })}>

              </header>
        );
};

Header.propTypes = {
    title: PropTypes.string ,
};

Header.defaultProps = {
    title: "Hello",
};

export default Header;
