import React from 'react';
import styles from './GoToTop.module.scss';
import useScrollPosition from "../../hooks/useScrollPosition";

const GoToTop = () => {

    const scrollY = useScrollPosition(50);
    if (scrollY < 150) return null;

    return (
        <>
            <button onClick={() => window.scrollTo({
                top: 0,
                behavior: "smooth"
            })} className={styles.root}>UP</button>
        </>
    )
}

GoToTop.propTypes = {};
GoToTop.defaultProps = {};

export default GoToTop;