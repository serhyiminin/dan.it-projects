import React from 'react';
import { Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage";
import PostPage from "./pages/PostPage";

const Routes = () => (
    <Switch>
        <Route exact path='/'><HomePage /></Route>
        <Route exact path='/post'><PostPage /></Route>
    </Switch>
)

export default Routes;