import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import Header from "./components/Header";
import Routes from "./Routes";

function App() {
  return (
      <BrowserRouter>
        <div className="App">
            <Header title="Context" />
            <Routes />
        </div>
      </BrowserRouter>
  );
}

export default App;
