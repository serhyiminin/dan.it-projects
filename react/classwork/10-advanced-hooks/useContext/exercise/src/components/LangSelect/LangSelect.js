import React, {useState} from 'react';

const LangSelect= () => {
        const [value, setValue] = useState('EN');

        return (
              <div>
                  <select
                      name="lang"
                      id="lang"
                      value={value}
                      onChange={({ target: { value } }) => setValue(value)}
                  >
                      <option value='EN'>EN</option>
                      <option value='DE'>DE</option>
                      <option value='FR'>FR</option>
                      <option value='UA'>UA</option>
                  </select>
              </div>
        );
};

export default LangSelect;
