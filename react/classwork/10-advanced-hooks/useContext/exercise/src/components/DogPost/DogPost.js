import React from 'react';
import PropTypes from "prop-types";

const DogPost = (props) => {
    const { currentLang } = props;

    return (
              <>
                  <img width={480} height={360} style={{ backgroundColor: 'lightgray' }} src="./DJ_Dog.gif" alt="DJ DOG"/>
                  { currentLang === 'EN' && <p>It's an edited picture of the dog (Indian Spitz) who is rocking on DJ.</p> }
                  { currentLang === 'DE' && <p>Es ist ein bearbeitetes Bild des Hundes (Indian Spitz), der auf DJ rockt.</p> }
                  { currentLang === 'FR' && <p>C'est une photo retouchée du chien (Indian Spitz) qui se balance sur DJ.</p> }
                  { currentLang === 'UA' && <p>Це відредагований малюнок собаки (індійського шпіца), який качає на DJ.</p> }
              </>
        );
};

DogPost.propTypes = {
    currentLang: PropTypes.string,
};

DogPost.defaultProps = {
    currentLang: 'EN',
}

export default DogPost;
