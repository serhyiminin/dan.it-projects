import React from 'react';
import AppContext from "./AppContext";
import PropTypes from "prop-types";

const AppContextProvider = ({value, children}) => {

    return (
        <AppContext.Provider value={value}>
            {children}
        </AppContext.Provider>
    )
}

AppContextProvider.propTypes = {
    value: PropTypes.any.isRequired,
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
}

export default AppContextProvider;