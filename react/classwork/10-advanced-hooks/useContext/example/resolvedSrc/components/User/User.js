import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import styles from './User.module.scss';
import AppContext from "../../context/AppContext";
import classNames from "classnames";

const User = (props) => {
    const { name, avatar } = props;
    const { themeStyles } = useContext(AppContext);

    if (!name) return null;

    return (
        <div className={classNames(styles.user, themeStyles.user)}>
            <h3>{name}</h3>
            <img src={avatar} alt={name} width={250} height={250}/>
        </div>
    )
}

User.propTypes = {
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string
};
User.defaultProps = {
    avatar: null,
};

export default User;