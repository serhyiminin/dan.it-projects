import React from 'react';
import { Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage";
import UsersPage from "./pages/UsersPage";

const Routes = () => (
    <Switch>
        <Route exact path='/'><HomePage /></Route>
        <Route exact path='/users'><UsersPage /></Route>
    </Switch>
)

export default Routes;