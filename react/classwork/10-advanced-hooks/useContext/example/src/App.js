import './App.scss';
import Header from "./components/Header";
import Routes from "./Routes";
import {BrowserRouter} from "react-router-dom";

function App() {
    return (
        <BrowserRouter>
                <div className="App">
                    <Header title="React Context"/>
                    <Routes/>
                </div>
        </BrowserRouter>
    );
}

export default App;
