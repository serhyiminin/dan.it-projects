import React from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import {NavLink} from "react-router-dom";

import {ReactComponent as DarkSVG} from "../../assets/dark.svg";
import {ReactComponent as LightSVG} from "../../assets/light.svg";

const Header = (props) => {
    const {title} = props;

    return (
        <header className={styles.root}>
            <span>{title}</span>
            <nav>
                <ul>
                    <li>
                        <NavLink to='/' exact activeClassName={styles.activeLink}>Home</NavLink>
                    </li>
                    <li>
                        <NavLink to='/users' activeClassName={styles.activeLink}>Users</NavLink>
                    </li>
                </ul>
            </nav>

            <button className={styles.themeBtn}>Change Theme</button>

        </header>
    );
};

Header.propTypes = {
    title: PropTypes.string,
};

Header.defaultProps = {
    title: "Hello",
};

export default Header;
