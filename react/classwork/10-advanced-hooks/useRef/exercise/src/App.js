import React, {useState} from 'react';
import classNames from 'classnames';
import styles from './App.module.scss';

function App() {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <div className={styles.App}>
            <div className={styles.header}>
                <button onClick={() => setIsOpen(prev => !prev)}>Search</button>
            </div>

            <div className={classNames(styles.searchContainer, { [styles.open]: isOpen })}>
                <input type="search" name="search"/>
                <button>OK</button>
            </div>
        </div>
    );
}

export default App;
