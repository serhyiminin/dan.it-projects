import React, {useEffect, useState} from 'react';
import PropTypes from "prop-types";

const DataSaver = ({ value }) => {
    const [fact, setFact] = useState('');

    useEffect(() => {
        (async () => {
            console.log('LOADING STARTED');
            const { fact } = await fetch('https://catfact.ninja/fact').then(res => res.json());
            setFact(fact);
            console.log('LOADING FINISHED');
        })()

    }, [value]);

    return <p>{fact}</p>;
}

DataSaver.propTypes = {
    value: PropTypes.bool.isRequired,
}


export default DataSaver;