import React from 'react';
import PropTypes from 'prop-types';
import styles from './ShowMore.module.scss';

// Если высота элемента больше 100px - показывать функционал showMore;

const ShowMore = ({ text }) => {
    return (
        <div className={styles.root}>
            <div className={styles.textWrapper}>
                <p className={styles.textContainer}>{text}</p>
            </div>
            <button type='button'>Show</button>
        </div>
    )
}

ShowMore.propTypes = {
    text: PropTypes.string.isRequired,
};

export default ShowMore;