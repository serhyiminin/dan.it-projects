import React from 'react';
import PropTypes from 'prop-types';
import {useField} from "formik";

const CustomInput = (props) => {
    const { type, label } = props;
    const [field, meta] = useField(props)
    console.log('!RERENDER ', field.name);
    
    return (
        <>
            <label>{label}</label>
            <input type={type} value={field.value} {...field} />
            <span className='error'>{meta.touched && meta.error ? meta.error : ''}</span>
        </>
    );
}

CustomInput.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string
};
CustomInput.defaultProps = {
    type: 'text',
    label: ''
};

export default CustomInput;