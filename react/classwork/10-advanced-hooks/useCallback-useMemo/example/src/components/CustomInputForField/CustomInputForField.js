import React from 'react';
import PropTypes from 'prop-types';

const CustomInputForField = (props) => {
    const { type, label, field, form } = props;

    const touched = form.touched[field.name];
    const error = form.errors[field.name];

    return (
        <>
            <label>{label}</label>
            <input type={type} value={field.value} {...field} />
            <span className='error'>{touched && error ? error : ''}</span>
        </>
    );
}

CustomInputForField.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string
};
CustomInputForField.defaultProps = {
    type: 'text',
    label: ''
};

export default CustomInputForField;