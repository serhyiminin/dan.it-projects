import styles from './App.module.scss';
import {useState} from "react";
import { Button } from "@mui/material";

function App() {
  return (
    <div className="App">
    <Button className={styles.btn} onClick={() => console.log('HELLO')} color='error' variant='contained'>I am button</Button>
    </div>
  );
}

export default App;
