1. Создание компонентов и `styled`;
2. Псевдоклассы;
3. Использование пропсов;
4. Ref на другие элементы;
5. Theme и ThemeProvider;

```js
const theme = {
    light: {
        background: '#fff',
        color: '#61dafb',
        borderColor: '#61dafb',
    },
    dark: {
        background: '#61dafb',
        color: '#fff',
        borderColor: '#fff',
    }
};

```