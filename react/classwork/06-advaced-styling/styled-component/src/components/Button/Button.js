import styled from "styled-components";

const StyledWrapper = styled.div`
  box-sizing: border-box;
  padding: 100px;
  border: 1px solid black;
`

const StyledButton = styled.button`
  padding: 6px 10px;
  box-sizing: border-box;
  background: ${props => props.background === 'red' ? '#ed2939' : '#61dafb'};
  color: #fff;
  outline: unset;
  filter: brightness(.9);
  cursor: pointer;
  margin: 10px;
  
  &:hover {
    background: #ed2939;
  }

  ${StyledWrapper}:hover & {
    background: #F48E0B;
  }
`;

const Button = ({ children }) => {
    return (
        <StyledWrapper>
            <StyledButton>{ children }</StyledButton>
        </StyledWrapper>
    )
}

export default Button;

