import React from "react";
import ItemCart from "../../components/Item";
import CartItem from "../../components/CartItem";

const CartPage = ({ cart, openModal }) => {

    console.log(cart);
    return (
        <>
            <h1>CART</h1>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {cart.map(element =>{
                    return <CartItem openModal={openModal} count={element.count} name={element.name}/>
                })}
            </div>
        </>
    )
}

export default CartPage;