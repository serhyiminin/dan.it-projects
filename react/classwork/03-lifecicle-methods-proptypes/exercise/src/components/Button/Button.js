import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import classNames from "classnames";
import styles from "./Button.module.scss";

class Button extends PureComponent {
  render() {
    const { children, type, onClick } = this.props;

    return (
      <button className={classNames(styles.btn)} type={type} onClick={onClick}>
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(["submit", "button", "reset"]),
};

// Button.defaultProps = {
//   children: PropTypes.oneOfType([
//     PropTypes.arrayOf(PropTypes.node),
//     PropTypes.node,
//   ]).isRequired,
//   onClick: () => {},
//   type: "button",
// };

export default Button;
