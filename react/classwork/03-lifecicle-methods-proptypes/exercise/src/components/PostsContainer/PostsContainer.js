import React, { PureComponent } from 'react';
import Post from "../Post";
import styles from './PostsContainer.module.scss';

class PostsContainer extends PureComponent {
    render(){
        const { posts } = this.props;

        return (
              <section className={styles.root}>
                  <h1>POSTS</h1>
                      <div className={styles.postsContainer}>
                        {posts.map(({id, ...args}) => <Post key={id} {...args} />)}
                      </div>
              </section>
        );
    }
};


export default PostsContainer;
