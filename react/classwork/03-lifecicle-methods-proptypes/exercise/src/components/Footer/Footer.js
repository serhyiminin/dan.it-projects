import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import Button from "../Button";
import styles from "./Footer.module.scss";

class Footer extends PureComponent {
  render() {
    const { title, year, onOrderFunc } = this.props;

    return (
      <footer className={styles.root}>
        <span>{title}</span>
        <span>FE-30, {year}</span>
        <Button onClick={onOrderFunc}>Order Call</Button>
      </footer>
    );
  }
}

Footer.propTypes = {
  onOrderFunc: PropTypes.any,
  title: PropTypes.any,
  year: PropTypes.any,
};

export default Footer;
