import React, { PureComponent } from 'react';
import { CircularProgress } from "@mui/material"; // https://mui.com/api/circular-progress/


class Preloader extends PureComponent {
    render(){
        const { color, size } = this.props;

        return <CircularProgress color={color} size={size} />;
    }
}


export default Preloader;
