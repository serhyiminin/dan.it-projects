import React from "react";
import style from "./Footer.module.scss";

const Footer = (props) => {
  const { title, countMessage } = props;
  return (
    <footer className={style.footer}>
      <h1 className={style.footerTitle}>{title}</h1>
      <span className={style.footerEmails}>Emails: {countMessage}</span>
    </footer>
  );
};

export default Footer;
