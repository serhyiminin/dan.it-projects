import './App.css';
import React from 'react';
import Modal from "./components/Modal/Modal";

class App extends React.Component {

  state = {
    isOpen: false,
  }

  openModal = () => {
      this.setState({
          isOpen: true,
      })
    }

    closeModal = () => {
        this.setState({
            isOpen: false,
        })
    }

  render (){
    return (
        <div className="App">
          <button onClick={(event) => {
              this.setState(current => ({...current, isOpen: !current.isOpen}));
              event.target.blur();
          }}>Open</button>
          {this.state.isOpen && <Modal
              closeModal={this.closeModal}
              backgroundColor="green"
              actions={<><button onClick={this.closeModal}>Hello</button></>}
          />}
        </div>
    );
  }
}

export default App;
