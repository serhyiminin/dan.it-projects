import React from "react";

const ContactsPage = () => {

    return (
        <section>
            <h1 >CONTACTS</h1>
            <ul>
                <li>
                    Tel: 12312431523
                </li>
                <li>
                    Email: some.email@gmail.com
                </li>
                <li>
                    City: Kyiv
                </li>
            </ul>
        </section>
    )
}

export default ContactsPage;