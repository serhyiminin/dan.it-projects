import React, {useEffect, useState} from "react";

function PostPage(props) {
    const { match: { params } } = props;

    const [post, setPost] = useState({});

    useEffect(() => (async () => {
        const result = await fetch(`https://ajax.test-danit.com/api/json/posts/${params.id}`)
            .then(res => res.json());
        setPost(result);
    })(), []);

    return (
        <section>
            {post.id &&
            <>
                <h2>{post.title}</h2>
                <p>{post.body}</p>
            </>}
        </section>
    )
}

export default PostPage;