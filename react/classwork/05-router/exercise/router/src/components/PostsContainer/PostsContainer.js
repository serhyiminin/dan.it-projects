import React from 'react';
import Post from "../Post";
import styles from './PostsContainer.module.scss';
import Preloader from "../Preloader";

const PostsContainer = (props) => {
        const { posts, isLoading, id, history } = props;

        return (
              <section className={styles.root}>
                      <div className={styles.postsContainer}>
                          {isLoading
                              ? <Preloader color="secondary" size={60} />
                              : <>{posts.map(({id, ...args}) => <Post history={history} key={id} id={id} {...args} />)}</>}
                      </div>
              </section>
        );
};

export default PostsContainer;
