import React from 'react';
import Button from "../Button";
import styles from './GoBack.module.scss';

const GoBack = () => {
    return (
        <div className={styles.root}>
            <Button>Go Back</Button>
        </div>
    );
}

export default GoBack;