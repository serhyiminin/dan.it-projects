import React from "react";
import Button from "../Button";
import styles from './NoMatch.module.scss';

const NoMatch = () => {
    return (
        <section>
            <h1>Sorry, we weren’t able to find the page you are looking for.</h1>
            <Button>Go Home</Button>
        </section>
    )
}

export default NoMatch;