import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import NoteItem from "../NoteItem";
import {useSelector, useDispatch} from "react-redux";
import {getData, setConfig, setConfigModal, setOpenModal} from "../../appStore/actionCreator/actionCreator";



const NoteContainer = () => {

    const dispatch = useDispatch()


    const items = useSelector(({toDo:{items}})=> items)
    console.log(items)
    useEffect(() => {
        dispatch(getData())
        // dispatch(setConfigModal('YFDJGAFDJG', <>
        //     <button>sldhfljshjdfs</button>
        // </>))
        // dispatch(setOpenModal(true))
    }, [])

    if (!items.length) return <p style={{ fontSize: 24 }}>You don't have any notes yet</p>;
    return (
        <ul>
            {items && items.map(({ text, id, isDone }, index) => <NoteItem index={index + 1} text={text} key={id} id={id} isDone={isDone} />)}
        </ul>
    )
}

NoteContainer.propTypes = {
    notes: PropTypes.array,
};
NoteContainer.defaultProps = {
    notes: null,
};

export default NoteContainer;