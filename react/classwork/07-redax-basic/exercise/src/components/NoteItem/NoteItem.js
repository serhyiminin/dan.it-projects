import React from 'react';
import classNames from "classnames";
import PropTypes from 'prop-types';
import styles from './NoteItem.module.scss';
import {Button, Checkbox, TextField} from "@mui/material";
import {ReactComponent as EditSVG} from "../../assets/edit.svg";
import {ReactComponent as DeleteSVG} from "../../assets/delete.svg";
import {removeNote, setConfigModal, setOpenModal, toggleIsDone} from "../../appStore/actionCreator/actionCreator";
import {useDispatch} from "react-redux";
// import { ReactComponent as CheckSVG } from "../../assets/check.svg";

const NoteItem = (props) => {
    const {index, text, isDone, id} = props;
    const dispatch = useDispatch();
    console.log(id)

    return (
        <li className={classNames(styles.root, {[styles.rootDone]: isDone})}>
            <div className={styles.wrapper}>
                <Checkbox
                    className={styles.checkbox}
                    value={isDone}
                    onChange={() => dispatch(toggleIsDone(id))}
                />
                <span>{index}.</span>
                <p className={classNames({[styles.done]: isDone})}>{text}</p>
            </div>

            <div className={styles.wrapper}>
                {!isDone && <Button variant='contained' className={styles.btn}><EditSVG/></Button>}
                <Button variant='contained' color='error' className={styles.btn} onClick={() => {
                    dispatch(setConfigModal(`You wont delete${text}?`,
                        <>
                            <button onClick={() => {
                                dispatch(removeNote(id))
                                dispatch(setOpenModal(false))
                            }}>Yes</button>
                            <button onClick={() => {
                                dispatch(setOpenModal(false))
                            }}>No</button>
                        </>))
                    dispatch(setOpenModal(true))
                }}><DeleteSVG/></Button>
            </div>
        </li>
    )
}

NoteItem.propTypes = {
    index: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    text: PropTypes.string,
    isDone: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
};
NoteItem.defaultProps = {
    text: '',
};

export default NoteItem;


// <li className={styles.rootEdit}>
//     <TextField label='Edit Note' className={styles.input} />
//     <Button variant='contained' color='success' className={styles.btn} onClick={() => setIsEdited(false)}><CheckSVG /></Button>
// </li>