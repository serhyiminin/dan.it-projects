import React from 'react';
import styles from './Modal.module.scss';
import { Button } from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {setOpenModal} from "../../appStore/actionCreator/actionCreator";

const Modal= () => {
    const { title, actions, isOpen } = useSelector(state => state.modal);
    const dispatch = useDispatch();

    if (!isOpen) return null;

        return (
             <div className={styles.root}>
                 <div className={styles.background} />
                 <div className={styles.content}>
                     <div className={styles.closeWrapper}>
                         <Button onClick={() => dispatch(setOpenModal(false))} variant="text" color="warning">Close</Button>
                     </div>
                     <h2>{title}</h2>
                     <div className={styles.buttonContainer}>{actions}</div>
                 </div>
             </div>
        );
};

export default Modal;
