import {ADD_NOTE, TOGGLE_ISDONE, GET_DATA, SET_IS_OPEN_MODAL, SET_CONFIG_MODAL, REMOVE_NOTE} from "../actions/actions";

const BASE_URL = 'http://localhost:3001/todo'

export const addNote = (text) => async(dispatch)=>{
    const result = await fetch('http://localhost:3001/todo', {
        method:"POST",
        body:JSON.stringify({
            text,
        }),
        headers:{
            "Content-Type":"application/json"
        }
    }).then(res=>res.json())
    dispatch({type:ADD_NOTE, payload:result.data})
}
export const toggleIsDone = (id) => ({ type: TOGGLE_ISDONE, payload: id });
export const getData = () => async (dispatch) => {
    const {data} = await fetch(BASE_URL).then(res => res.json())

    dispatch({type: GET_DATA, payload: data})
}

export const setOpenModal = (isOpen) => ({
    type: SET_IS_OPEN_MODAL,
    payload: isOpen,
})

export const setConfigModal = (title, actions) => ({type: SET_CONFIG_MODAL, payload: {title, actions} })

export const removeNote = (id) => async(dispatch) => {
    const {data} = await fetch(BASE_URL+`?id=${id}`,{
        method:"DELETE",
    }).then(res => res.json())
    dispatch({type: REMOVE_NOTE, payload: data})
}