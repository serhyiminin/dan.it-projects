import { INCREMENT_COUNTER, CLEAR_COUNTER, DECREMENT_COUNTER, SET_VALUE_COUNTER } from "../actions";

const initialState = {
    counter: 0,
}

const counterReducer = (state= initialState, action) => {
    switch (action.type) {
        case INCREMENT_COUNTER: // Если action.type === 'INCREMENT_COUNTER'
            return {...state, counter: state.counter + 1}; // Перезапиши в state новое значение (такое)
        case DECREMENT_COUNTER:// Если action.type === 'DECREMENT_COUNTER'
            return {...state, counter: state.counter - 1}; // Перезапиши в state новое значение (такое)
        case CLEAR_COUNTER:
            return {...state, counter: 0};
        case SET_VALUE_COUNTER:
            if (Number.isNaN(Number(action.payload)) || !action.payload) {
                return state;
            }
            return {...state, counter: +action.payload};
        default:
            return state;
    }
}

export default counterReducer;