import React from 'react';
import Counter from "../../components/Counter";


const CounterPage = () => (
        <>
            <h1>Counter</h1>
            <Counter />
        </>
);


export default CounterPage;