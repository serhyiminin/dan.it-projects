import React from 'react';
import styles from './CatFactsContainer.module.scss';
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {addFacts} from "../../store/actionCreators";

const CatFactsContainer = () => {
    const dispatch = useDispatch();
    const { facts, isLoading } = useSelector(({ facts }) => facts, shallowEqual);

    console.log(facts)

    return (
        <div className={styles.root}>
            <button onClick={() => dispatch(addFacts())}>{isLoading ? 'Loading...' : 'Get new awesome fact!'}</button>
            {facts && facts.map(item => <p key={item}>{item}</p>)}
        </div>
    )
}

export default CatFactsContainer;