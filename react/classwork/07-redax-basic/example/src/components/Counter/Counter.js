import React, { useState } from 'react';
import styles from './Counter.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {clear, decrement, increment, setValue} from "../../store/actionCreators";

const Counter = () => {
    const [inputValue, setInputValue] = useState('');

    const state = useSelector((state) => state);
    const counter = useSelector(({ counter }) => counter.counter);
    console.log(state);

    const dispatch = useDispatch();

    const incrFunc = () => {
        dispatch(increment());
    }

    const decrFunc = () => {
        dispatch(decrement());
    }

    const clearFunc = () => {
        dispatch(clear());
    }

    const setValFunc = () => {
        dispatch(setValue(inputValue));
        setInputValue('');
    }

    return (
        <div className={styles.root}>
            <p className={styles.counter}>{counter}</p>
            <div className={styles.btnWrapper}>
                <button onClick={incrFunc}>+</button>
                <button onClick={decrFunc}>-</button>
                <button onClick={clearFunc}>Clear counter</button>
            </div>

            <div className={styles.btnWrapper}>
                <input
                    type="text"
                    value={inputValue}
                    onChange={({ target }) => setInputValue(target.value)}
                />
                <button onClick={setValFunc}>Set Value</button>
            </div>
        </div>
    )
}

export default Counter;