# TODO API

#### URL: http://localhost:3001/
<hr>


## POST /sign-in

#### Body:
```js
{
    email: '',
    password: '',
}
```

#### Responses:

status: 200
```js
{
    status: 'success',
    data: {
        user: {
            name: '',
            avatar: '',
        },
        token: ''
    }
}
```
<hr>


## GET /items
#### Headers:
`"Authorization": "Bearer HGHGhkgakfgkgJGj123j12j3gjkGJGj12g3j1g2j3gJGJGSjg1j23gj12hg3j1h23"`

#### Responses:

status: 200
```js
{
    status: 'success',
    data: [
        {
            title: '',
            img: '',
            description: '',
            isFavorite: false,
            id: '',
        }
    ],
}
```
<hr>


## PATCH /toggle-favorite
#### Headers:
`"Authorization": "Bearer HGHGhkgakfgkgJGj123j12j3gjkGJGj12g3j1g2j3gJGJGSjg1j23gj12hg3j1h23"`


#### Body:
```js
{
    id: '',
}
```

#### Responses:

status: 200
```js
{
    status: 'success',
    data: {
        title: '',
        img: '',
        description: '',
        isFavorite: true,
        id: '',
    }
}
```
<hr>