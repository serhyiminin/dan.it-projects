const {items, token} = require('../../data');
const {sendBadResponse, sendGoodResponse} = require("../utils");

const getItems = (app) => {
    app.get('/items', (req, res) => {
        const reqToken = req.header('Authorization');
        if (!reqToken) {
            sendBadResponse(res, 401, `Authorization header is required`);
            return;
        }

        const clearedReqToken = reqToken.replace(/Bearer\s/gi, '').trim();
        if (clearedReqToken !== token) {
            sendBadResponse(res, 401, `Authorization token is incorrect`);
            return;
        }

        sendGoodResponse(res, items);
    })
}
module.exports = getItems;