import { user, token } from "../../data";
import { sendBadResponse, sendGoodResponse } from "../utils";
import { emailRegExp } from "../../constants/regExp";

const signIn = (app) => {
  app.post("/sign-in", (req, res) => {
    if (!req.body) {
      sendBadResponse(res, 400, `Body is required.`);
      return;
    }

    if (!req.body?.email) {
      sendBadResponse(res, 400, `Email is required.`);
      return;
    }

    if (!emailRegExp.test(req.body?.email)) {
      sendBadResponse(res, 400, `Invalid format of email`);
      return;
    }

    if (!req.body?.password) {
      sendBadResponse(res, 400, `Password is required.`);
      return;
    }

    sendGoodResponse(res, { user, token });
  });
};
export default signIn;
