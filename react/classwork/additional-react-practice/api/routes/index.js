const signIn = require('./sign-in');
const getItems = require('./getItems');
const toggleIsFavorite = require('./toggleIsFavorite');
module.exports = (app) => {
    signIn(app);
    getItems(app);
    toggleIsFavorite(app);
}

