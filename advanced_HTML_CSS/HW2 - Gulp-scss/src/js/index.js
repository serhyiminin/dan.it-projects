const burg = document.querySelector(`.header__container-burger-icon`);
const menu = document.querySelector(`.header__container-navigation-wrapper`);

burg.addEventListener(`click`, (e) => {
  const collection = e.currentTarget.children;
  for (const bar of collection) {
    bar.classList.toggle("change");
  }
  menu.classList.toggle("hide");
  console.log(e.currentTarget.children);
});
