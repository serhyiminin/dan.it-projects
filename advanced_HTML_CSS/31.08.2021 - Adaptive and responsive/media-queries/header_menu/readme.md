Дан макет хедера и меню в трех расширениях (для мобильного, планшета, десктопа) 

https://www.figma.com/file/VDT64mrTXppGWa7moCyLmZ/Adaptive-header-menu?node-id=0%3A1

Сверстайте этот макет используя SCSS, ```media-queries```, подход "mobile-first" и методологию БЭМ
