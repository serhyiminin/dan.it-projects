//Добавьте JavaScript к кнопке button, чтобы при нажатии элемент <div id="text"> исчезал.
/*const div = document.querySelector(`#hidebutton`);
const text = document.querySelector(`#text`);
div.addEventListener(`click`, () => text.classList.toggle(`hidden`));
div.addEventListener(`click`, (event) => (event.target.style.display = `none`));

const btn = document.createElement(`button`);
btn.innerHTML = `Увійти`;
btn.classList.add(`btn`);
document.body.appendChild(btn);
btn.onclick = () => {
  alert('Welcome!');
};
btn.addEventListener(
  `mouseover`,
  () => {
    alert(`При клике по кнопке вы войдёте в систему`);
  },
  { once: true },
);
*/
const PHRASE = `Добро пожаловать`;

/**
 * Задание 3.
 *
 * Создать элемент h1 с текстом «Добро пожаловать!».
 *
 * Под элементом h1 добавить элемент button c текстом «Раскрасить».
 *
 * При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
 */
const hi = document.createElement(`h1`);
hi.innerHTML = PHRASE;
const btn = document.createElement(`button`);
btn.innerHTML = 'Розфарбувати';
document.body.appendChild(hi);
document.body.appendChild(btn);

const colorChanger = () => {
  let str = hi.innerHTML;
  hi.innerText = '';
  for (let char of str) {
    let span = document.createElement(`span`);
    span.innerHTML = char;
    span.style.color = gerRandomColor();
    hi.append(span);
  }
};
function gerRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r},${g},${b})`;
}
btn.addEventListener(`click`, colorChanger);
btn.addEventListener(`mousemove`, colorChanger);
