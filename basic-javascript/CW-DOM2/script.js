/**
 * Задание 1.
 *
 * Получить элемент с классом .remove.
 * Удалить его из разметки.
 * .remove()
 *
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 * .classList.add(class)
 * .classList.remove(class)
 *
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах: в одну строку и в две строки.
 * .classList.replace(oldClass, newClass)
 */

/* Удаление */
let elem = document.getElementsByClassName('remove');
elem[0].remove();

/* Изменение: способ 1 */
// let elem2 = document.getElementsByClassName('bigger');
// elem2[0].classList.add('active');
// elem2[0].classList.remove('bigger');

/* Изменение: способ 2 */
let elem3 = document.getElementsByClassName('bigger');
elem3[0].classList.replace('bigger', 'active');
