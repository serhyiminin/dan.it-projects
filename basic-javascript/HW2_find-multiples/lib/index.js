button.onclick = function () {
  let inputNumber;

  do {
    inputNumber = prompt("Введіть будь яке число");
  } while (isNaN(inputNumber));

  //Перевірка на введення великого числа зроблена навмисно, але за умовами ДЗ її немає, тому виключена
  // if (+inputNumber > 1000) {
  //   if (!confirm(`Ви впевнені, що бажаєте ввести ${inputNumber}?`)) {
  //     return;
  //   }
  // }

  let counter = 0;
  for (let i = 1; i <= inputNumber; i++) {
    if (i % 5 === 0) {
      console.log(i);
      counter++;
    }
  }

  if (counter === 0) {
    console.log("Sorry, no numbers");
  }

  //Друга частина завдання

  let pauseTime = 3000;
  console.log(
    `Виконання другої частини завдання продовжиться за ${pauseTime} мілісекунд.`
  );
  setTimeout(() => {
    console.log("Продовження виконання");

    let numberOne;
    let numberTwo;

    do {
      numberOne = +prompt("Введіть ПЕРШЕ просте ціле число");
    } while (
      isNaN(numberOne) ||
      isSimple(numberOne) === false ||
      numberOne < 2
    );

    do {
      numberTwo = +prompt("Введіть ДРУГЕ просте ціле число");
    } while (
      isNaN(numberTwo) ||
      isSimple(numberTwo) === false ||
      numberTwo < 2
    );

    if (numberOne > numberTwo || numberOne === numberTwo) {
      alert(
        `Перше число - ${numberOne} більше або дорівнює другому - ${numberTwo}. Почніть спочатку`
      );
      return;
    } else {
      for (let i = numberOne; i <= numberTwo; i++) {
        if (isSimple(i) === true) {
          console.log(i);
        }
      }
    }
    console.log("********* THE END *********");
  }, pauseTime);

  function isSimple(num) {
    let flag = true;
    for (let i = 2; i < num; i++) {
      if (num % i === 0) {
        flag = false;
        break;
      }
    }
    return flag;
  }
};
