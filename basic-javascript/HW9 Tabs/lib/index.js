/*
Технические требования:

В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.
*/
const navContainer = document.querySelector(`.tabs`);
const navList = Array.from(document.querySelectorAll(`.tabs-title`));
const pList = Array.from(document.querySelectorAll(`.tabs-content p`));
let current = pList[0];
current.style.display = "block";

navContainer.addEventListener(`click`, (event) => {
  navList.forEach((elem) => {
    elem.classList.remove(`active`);
  });
  event.target.classList.add(`active`);
  let index = navList.indexOf(event.target);
  pList[index].style.display = "block";
  current.style.display = `none`;
  current = pList[index];
});
