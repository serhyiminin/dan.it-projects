button.onclick = function () {
  /*Технические требования:

Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
Считать с помощью модального окна браузера число, которое введет пользователь (n).
С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).
*/

  //The second number mast be greater than first one
  let first = 8;
  let second = 21;

  do {
    n = prompt('Введіть ціле число');
    n = Math.round(n);
  } while (isNaN(Number(n)) || n === null);

  function fibonacci(first, second, n) {
    n = n - 2;

    while (n > 0) {
      let third = first + second;
      first = second;
      second = third;
      n = n - 1;
      fibonacci(first, second, n);
    }
    return second;
  }

  function fibonacciMinus(first, second, n) {
    n = n - 1;

    while (n < 0) {
      let third = second - first;
      second = first;
      first = third;
      n = n + 1;
      fibonacci(first, second, n);
    }
    return second;
  }

  if (n === '' || n === 0) {
    console.log('ESC pressed or 0 entered');
  } else {
    if (n > 0) {
      console.log(
        `${Math.round(n)}-th Fibonacci number in the sequence = ` +
          fibonacci(first, second, n),
      );
    } else {
      console.log(
        `With ${Math.round(n)} Fibonacci number in the sequence = ` +
          fibonacciMinus(first, second, n),
      );
    }
  }
};
