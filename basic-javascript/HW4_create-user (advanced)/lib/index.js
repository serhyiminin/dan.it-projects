button.onclick = function () {
  /*
Необязательное задание продвинутой сложности:
Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.
*/
  const firstNameString = "Tell the student's First name";
  const lastNameString = "Tell the student's Last name";
  let flag = false;
  let err = "";

  const user = createNewUser();

  if (err !== "ESC pressed or invalid names entered") {
    console.log(user.getLogin);
  } else {
    console.log(err);
  }

  function createNewUser() {
    let firstName;
    let lastName;
    let newUser = {};

    do {
      firstName = getData(firstNameString);

      if (firstName === null) {
        break;
      } else {
        lastName = getData(lastNameString);

        if (lastName === null) {
          break;
        } else {
          flag = true;
        }
      }
    } while (flag === false);

    if (firstName === null || lastName === null) {
      err = "ESC pressed or invalid names entered";
      return;
    }

    Object.defineProperty(newUser, "_firstName", {
      writable: false,
      value: firstName,
    });

    Object.defineProperty(newUser, "_lastName", {
      writable: false,
      value: lastName,
    });

    Object.defineProperty(newUser, "getLogin", {
      get() {
        let login = (this._firstName.charAt(0) + this._lastName).toLowerCase();
        return login;
      },
    });

    Object.defineProperty(newUser, "setFirstName", {
      set(name) {
        this._firstName = name;
      },
    });

    Object.defineProperty(newUser, "setLastName", {
      set(surname) {
        this._lastName = surname;
      },
    });
    return newUser;
  }
}; //END of click function

function getData(string) {
  let data = prompt(string);

  if (
    data === "" ||
    data === null ||
    data === undefined ||
    typeof Number(data) !== "number" ||
    !isNaN(data)
  ) {
    return null;
  } else {
    data = data.trim();
    data = data.replace(/\s+/g, " ");
    return data;
  }
}
