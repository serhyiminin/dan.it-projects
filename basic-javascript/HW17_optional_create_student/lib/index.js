button.onclick = function () {
  /*
Технические требования:

Создать пустой объект student, с полями name и lastName.
Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.

Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.
*/
  const firstNameString = "Tell the student's First name";
  const lastNameString = "Tell the student's Last name";
  const subjectString = "Tell the SUBJECT";
  const markString = "Tell your subject GRADE";
  let student = {
    name: "",
    lastName: "",
  };

  do {
    let firstName = getData(firstNameString);

    if (firstName === null) {
      break;
    } else {
      student.name = firstName;
      lastName = getData(lastNameString);

      if (lastName === null) {
        break;
      } else {
        student.lastName = firstName;
      }
    }
  } while (student.name === "" || student.lastName === "");

  if (student.name === "" || student.lastName === "") {
    console.log("ESC has been pressed or invalid Names entered");
    return;
  }

  Object.defineProperty(student, "tabel", {
    value: [],
  });

  do {
    let subject = getData(subjectString);

    if (subject === null) {
      break;
    } else {
      let mark = getMark(markString);

      if (mark === null) {
        break;
      } else {
        student.tabel.push([subject, mark]);
      }
    }
  } while (true);

  if (student.tabel.length == 0) {
    console.log("ESC has been pressed or invalid data entered");
    return;
  }

  if (student.tabel.filter((m) => m[1] < 4).length == 0) {
    console.log("The student has enrolled in the next-level course.");

    const marks = student.tabel.map((e) => e[1]);
    const summ = marks.reduce((acc, e) => acc + e, 0);

    if (summ / marks.length > 7) {
      console.log("The student has awarded a merit-based scholarship");
    }
  } else {
    console.log(
      "Due to poor academic performance, the student hasn't be enrolled in the next-level course"
    );
  }
}; //END of click function

function getData(string) {
  let data = prompt(string);

  if (
    data === "" ||
    data === null ||
    data === undefined ||
    typeof Number(data) !== "number" ||
    !isNaN(data)
  ) {
    return null;
  } else {
    data = data.trim();
    data = data.replace(/\s+/g, " ");
    return data;
  }
}
function getMark(string) {
  let data = prompt(string);

  if (
    data === "" ||
    data === null ||
    data === undefined ||
    typeof Number(data) !== "number" ||
    isNaN(data)
  ) {
    return null;
  } else {
    return +data;
  }
}
