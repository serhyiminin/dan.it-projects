const resultContainer = document.querySelector('.result')
/**
 * Функция evyjltybz двух чисел
 */

function getNumber (a, b) {
    return a*b;

}
resultContainer.innerHTML= getNumber(4,6)



/**
 * Функция принимает имя и возвращает строку 'Привет, {Имя}'
 */


/**
 * Функция принимает вид животного (кошка или собака) и возвращает его звук (лай или мяуканье)'
 */
function getAnimal (a){
    //a===`кошка` ? return `Мяу-Мяу` : return `Гав-Гав`;
    if(a===`кошка`){
        return `Мяу-Мяу`}
    else if(a===`собака`)
    { return `Гав-Гав`}
    else
    {return `Ця тварина на шумить`};
}

resultContainer.innerHTML = getAnimal (`кошка`);
/**
 * Функция принимает длину радиуса круга и возвращает его площадь'
 */
// () => {}
const getRadius = radius => radius**2 * Math.PI
resultContainer.innerHTML = getRadius(10);

/**
 * Функция принимает ширину и высоту прямоугольника, возвращает true если это квадрат и false если нет'
 */
const getParametrs = (width, height) =>  width === height

resultContainer.innerHTML = getParametrs(10,20);
/**
 * Функция принимает ширину и высоту прямоугольника и возвращает его периметр если это не квадрат'
 */

const getParam = (width, height) => {
    if (width !== height){
        return (width + height)*2
    }
}

resultContainer.innerHTML = (getParam(20, 25 ))
/**
 * Функция принимает аргументы ключа и значения и возвращает объект с этими ключом, значением'
 */
const keyObj = function (key, value) {
    let newObj ={}
    newObj[key] = value;
    console.log(newObj)
}

keyObj('name', 'Stanislav');
/**
 * Функция принимает строки в неограниченном колличестве и все их выводит одной строкой через разделитель " | "'
 */

const getString = (...arg) => {
    return arg.join(" | ");
}
resultContainer.innerHTML = getString('Bla-bla', 'Abbbb','Gggg')
/**
 * Функция принимает Имя и возвращает HTML тег <span> с этим именем красного цвета'
 */
function getName (name) {
    return `<span style="color: red" >${name}</span>`
}

resultContainer.innerHTML = getName('Stanislav')

/**
 * Функция принимает css селектор, находит первый элемент и добавляет к нему клас анимации "pulse"'
 */

const setAnimation = (selector) => {
    const elem = document.querySelector(selector);
    elem.classList.add('pulse');
};

setAnimation('span');

/**
 * Функция принимает css селектор, находит первый элемент и добавляет к нему обработчик события "click".
 * При клике выводится alert с внутренним текстом элемента'
 */

const setOnCLick= (selector) => {
    const elem = document.querySelector(selector);

    elem.addEventListener('click', () => {
        alert(elem.innerText)
    });
};

setOnCLick('span');
