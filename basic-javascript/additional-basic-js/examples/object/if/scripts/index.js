let currentCategory = "red";
const container = document.querySelector(".container");
const colorOblect = {
  red: "#b90e0a",
  blue: "#1357a6",
  green: "#149414",
  yellow: "#ffdc00",
  orange: "#ff6600",
  violet: "#723189",
  white: "#ffffff",
  black: "#000000",
};

const changeActiveTabStyle = (target) => {
  document
    .querySelectorAll("li")
    .forEach((item) => item.classList.remove("active"));
  target.classList.add("active");
};

const setBackgroundColor = (color, container) => {
  //   console.log(color);
  //   console.log(color.colorOblect[color]);
  container.style.background = colorOblect[color];

  //   if (color === "red") {
  //     container.style.background = "#b90e0a";
  //   } else if (color === "blue") {
  //     container.style.background = "#1357a6";
  //   } else if (color === "green") {
  //     container.style.background = "#149414";
  //   } else if (color === "yellow") {
  //     container.style.background = "#ffdc00";
  //   } else if (color === "orange") {
  //     container.style.background = "#ff6600";
  //   } else if (color === "violet") {
  //     container.style.background = "#723189";
  //   } else if (color === "white") {
  //     container.style.background = "#ffffff";
  //   } else if (color === "black") {
  //     container.style.background = "#000000";
  //   }
};

document.querySelector("ul").addEventListener("click", (event) => {
  const target = event.target;
  if (target.innerText === currentCategory || target === event.currentTarget) {
    return;
  }

  currentCategory = target.innerText;
  changeActiveTabStyle(target);
  setBackgroundColor(currentCategory, container);
});
