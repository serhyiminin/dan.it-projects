// Объявление и методы обращения к объекту

// const backpack = new Object();

// const backpack = {
//   height: 150,
//   width: 70,
//   color: "green",
//   material: "leather",
//   pockets: {
//     front: 1,
//     side: 2,
//   },
// };

// console.log(backpack);

// ===============================================================================

// const furniture = ["buttons", "zippers"];

// const backpack = {
//   height: 150,
//   width: 70,
//   color: "green",
//   material: "leather",
//   pockets: {
//     front: 1,
//     side: 2,
//   },
//   furniture,
// };
// console.log(backpack);
// ===============================================================================

// const createObject = () => {
//   const key = prompt("Введите ключ");
//   const value = prompt("Введите значение");

//   return {
//     [key]: value,
//   };
// };

// console.log(createObject());

// const jokes = [
//   {
//     id: 163,
//     type: "general",
//     setup: "What did one nut say as he chased another nut?",
//     punchline: " I'm a cashew!",
//   },
//   {
//     id: 277,
//     type: "general",
//     setup: "What was the pumpkin’s favorite sport?",
//     punchline: "Squash.",
//   },
//   {
//     id: 257,
//     type: "general",
//     setup: "What is red and smells like blue paint?",
//     punchline: "Red paint!",
//   },
//   {
//     id: 314,
//     type: "secondary",
//     setup: "Why are ghosts bad liars?",
//     punchline: "Because you can see right through them!",
//   },
//   {
//     id: 145,
//     type: "general",
//     setup: "How many South Americans does it take to change a lightbulb?",
//     punchline: "A Brazilian",
//   },
//   {
//     id: 183,
//     type: "secondary",
//     setup: "What did the Dorito farmer say to the other Dorito farmer?",
//     punchline: "Cool Ranch!",
//   },
//   {
//     id: 309,
//     type: "general",
//     setup: "Which side of the chicken has more feathers?",
//     punchline: "The outside.",
//   },
//   {
//     id: 182,
//     type: "general",
//     setup: "What did the dog say to the two trees?",
//     punchline: "Bark bark.",
//   },
//   {
//     id: 264,
//     type: "secondary",
//     setup: "What kind of award did the dentist receive?",
//     punchline: "A little plaque.",
//   },
//   {
//     id: 324,
//     type: "general",
//     setup: "Why couldn't the kid see the pirate movie?",
//     punchline: "Because it was rated arrr!",
//   },
//   {
//     id: 66,
//     type: "general",
//     setup: "Did you hear the story about the cheese that saved the world?",
//     punchline: "It was legend dairy.",
//   },
// ];

/* Напишите функцию которая преобразовывает массив шуток в объект где:
   ключ = строка id_type;
   значение = строка setup - punchline

   Пример:
   {
        66_general: 'Did you hear the story about the cheese that saved the world? - It was legend dairy.',
        264_secondary: 'What kind of award did the dentist receive? - A little plaque.',
        ...
   }
*/
/*
// function createObject(array) {
//   const newObject = {};
//   array.forEach((element) => {
//     // newObject[
//     //   `${element.id}_${element.type}`
//     // ] = `${element.setup}_${element.punchline}`;

//     newObject[
//         element.id + `_` + element.type
//       ] = element.setup +`_`+ element.punchline;

//   });
//   return newObject;
// }
// console.log(createObject(jokes));
// /*
// Задача IF

// =====================================================================================
// "key" in object

// const backpack = {
//     height: 150,
//     width: 70,
//     color: 'green',
//     material: 'leather',
//     pockets: {
//         front: 1,
//         side: 2,
//     },
// };

// console.log('height' in backpack);
// console.log('dog' in backpack);

// =====================================================================================
// Цикл «for…in»

// const backpack = {
//   height: 150,
//   width: 70,
//   color: "green",
//   material: "leather",
//   pockets: {
//     front: 1,
//     side: 2,
//   },
// };

// for (let key in backpack) {
//   console.log(key);
//   console.log(backpack[key]);
// }

// Object.keys(obj);
// Object.values(obj);

// const backpack = {
//   height: 150,
//   width: 70,
//   color: "green",
//   material: "leather",
//   pockets: {
//     front: 1,
//     side: 2,
//   },
// };

// console.log(Object.keys(backpack));
// console.log(Object.values(backpack));
// Object.keys(backpack).forEach((e) => console.log(backpack[e]));
*/
// =====================================================================================
// Сравнение

// Объекты равны только если это ссылка на один и тот же объект;

// const myFirstBackpack = {
//     height: 150,
//     width: 70,
//     color: 'green',
//     material: 'leather',
//     pockets: {
//         front: 1,
//         side: 2,
//     },
// };
//
// const mySecondBackpack = myFirstBackpack;
//
// console.log(mySecondBackpack === myFirstBackpack);

// const myFirstBackpack = {
//     height: 150,
//     width: 70,
//     color: 'green',
//     material: 'leather',
//     pockets: {
//         front: 1,
//         side: 2,
//         inner: {
//             small: 1,
//             laptop: 1,
//         }
//     },
// };
//
// const mySecondBackpack = {
//     height: 150,
//     width: 70,
//     color: 'green',
//     material: 'leather',
//     pockets: {
//         front: 1,
//         side: 2,
//         inner: {
//             small: 1,
//             laptop: 1,
//         }
//     },
// };

// console.log(mySecondBackpack === myFirstBackpack);

// /*
//  Напишите функцию для глубоко сравнения объектов
//  */

//
//
// const isObjectEqual = () => {
//
// };
//
// console.log(isObjectEqual(myFirstBackpack, mySecondBackpack));

// =====================================================================================
// Копирование

// "Object.assign(target, obj, obj, ...obj)"

// const backpack = {
//     height: 150,
//     width: 70,
//     color: 'green',
//     material: 'leather',
//     pockets: {
//         front: 1,
//         side: 2,
//         inner: {
//             small: 1,
//             laptop: 1,
//         }
//     },
// };

// const backpackCopy = Object.assign({}, backpack);
//
// console.log(backpack)
// console.log(backpackCopy)

// backpack.width = 100;
// backpack.pockets.front = 100;

// "Spread"
// { ...obj }

// "JSON"
// JSON.parse(JSON.stringify(obj))

// /*
// Напишите функцию для глубоко копирования объектов
//  */

// const objectDeepClone = () => {
//
// };

// =====================================================================================
// defineProperty, freeze

/**
Object.defineProperty(obj, prop, descriptor)

obj - Объект, в котором объявляется свойство.
prop - Имя свойства, которое нужно объявить или модифицировать.
descriptor - Дескриптор – объект, который описывает поведение свойства.


value – значение свойства, по умолчанию undefined
writable – значение свойства можно менять, если true. По умолчанию false.
configurable – если true, то свойство можно удалять, а также менять его в дальнейшем при помощи новых вызовов defineProperty.
 По умолчанию false.
enumerable – если true, то свойство просматривается в цикле for..in и методе Object.keys().
    По умолчанию false.
get – функция, которая возвращает значение свойства. По умолчанию undefined.
set – функция, которая записывает значение свойства. По умолчанию undefined.

Чтобы избежать конфликта, запрещено одновременно указывать значение value и функции get/set.
Либо значение, либо функции для его чтения-записи, одно из двух. Также запрещено и не имеет смысла
указывать writable при наличии get/set-функций.
 */

// const backpack = {
//     height: 150,
//     width: 70,
//     color: 'green',
//     material: 'leather',
//     pockets: {
//         front: 1,
//         side: 2,
//         inner: {
//             small: 1,
//             laptop: 1,
//         }
//     },
// };

// Object.defineProperty(backpack, 'height', {
//     value: 300,
// })

/* ------------------------------------- */

// Object.defineProperty(backpack, 'height', {
//     writable: false,
// });
// backpack.height = 300;

/* ------------------------------------- */

// Object.defineProperty(backpack, 'height', {
//     configurable: false,
// });
// backpack.height = 300;
//
// delete backpack.height;

/* ------------------------------------- */

// Object.defineProperty(backpack, 'height', {
//     enumerable: false,
// });
//
// console.log(Object.keys(backpack));
// console.log(backpack.height);

/* ------------------------------------- */

// console.log(backpack);

/**
Метод Object.freeze() замораживает объект:
это значит, что он предотвращает добавление новых свойств к объекту,
удаление старых свойств из объекта и изменение существующих свойств
или значения их атрибутов перечисляемости, настраиваемости и записываемости.
В сущности, объект становится эффективно неизменным.
Метод возвращает замороженный объект.
 */
//
// const backpack = {
//     height: 150,
//     width: 70,
//     color: 'green',
//     material: 'leather',
//     pockets: {
//         front: 1,
//         side: 2,
//         inner: {
//             small: 1,
//             laptop: 1,
//         }
//     },
// };
//
// const frozenBackpack = Object.freeze(backpack);
//
// console.log('backpack', backpack);
// console.log('frozenBackpack', frozenBackpack);
//
// backpack.height = 400;
//
// console.log('backpack', backpack);
// console.log('frozenBackpack', frozenBackpack);
//
// delete backpack.height;
//
// console.log('backpack', backpack);
// console.log('frozenBackpack', frozenBackpack);
//
// backpack.newKey = 'newValue';
//
// console.log('backpack', backpack);
// console.log('frozenBackpack', frozenBackpack);
//

/** Но */

// backpack.pockets.front = 1000000;
//
// console.log('backpack', backpack);
// console.log('frozenBackpack', frozenBackpack);
//
//
// /* ----------------------------------- */
//
// console.log(Object.isFrozen(backpack));

// /*
// Напишите функцию для глубокой заморозки объектов
//  */

// const objectDeepFreeze = () => {
//
// };

// =====================================================================================
// Методы объекта, this

// const backpack = {
//     height: 150,
//     width: 70,
//     isOpened: false,
//     open: function() {
//         console.log('Opened')
//     },
//     close: function() {
//         console.log('Closed')
//     }
// };
//
// backpack.open();
// backpack.close();

// =====================================================================================

//
// const backpack = {
//     height: 150,
//     width: 70,
//     isOpened: false,
//     open() {
//         console.log('Opened')
//     },
//     close() {
//         console.log('Closed')
//     }
// };
//
// backpack.open();
// backpack.close();

// =====================================================================================

// const open = function () {
//     console.log('Opened')
// }
// const close = function () {
//     console.log('Closed')
// }
//
// const backpack = {
//     height: 150,
//     width: 70,
//     isOpened: false,
//     open,
//     close,
// };
//
// backpack.open();
// backpack.close();

// =====================================================================================
// this

// let backpack = {
//     height: 150,
//     width: 70,
//     isOpened: false,
//     open: function() {
//         console.log('Opened');
//         this.isOpened = true;
//         return this.isOpened;
//     },
//     close: function() {
//         console.log('Closed');
//         this.isOpened = false;
//         return this.isOpened;
//     }
// };
//
// const anotherBackpack = backpack;
//
// backpack = {}

// =====================================================================================
// Рассмотрим глубже

/**
 * 1 - object
 * 2 - .
 * 3 - method
 * 4 - ()
 *
 * object.method()
 * this - объект перед точкой во время вызова
 */

// const dog = {
//     size: 'small',
//     kind: 'cute',
//     voice: 'bow wow',
//     friends: {
//       anotherDog: {
//           size: 'big',
//           kind: 'cute',
//           voice: 'woof woof',
//           say() {
//               alert(this.voice);
//           },
//           getParams (){
//               console.log(`It's a ${this.size} ${this.kind}`)
//           }
//       },
//     },
//     say() {
//         alert(this.voice);
//     },
//     getParams (){
//         console.log(`It's a ${this.size} ${this.kind} dog`)
//     }
// };

/**
 * dog.say() - this === dog;
 * dog.friends.anotherDog.say() - this === anotherDog;
 */

//
// const say = function (){
//     alert(this.voice);
// }
//
// const getParams = function (){
//     console.log(`It's a ${this.size} ${this.kind}`)
// }
//
// const dog = {
//     size: 'small',
//     kind: 'cute',
//     voice: 'bow wow',
//     friends: {
//         anotherDog: {
//             size: 'big',
//             kind: 'cute',
//             voice: 'woof woof',
//             say,
//             getParams,
//         },
//     },
//     say,
//     getParams,
// };

// ====================================================================================
/**
 * this свободно и доступно из любой функции
 */

// const anyFunctions = function (){
//     // "use strict";
//     console.log(this);
// }
//
// anyFunctions();

/** Но */

// const obj = {
//     anyFunctions,
// }
//
// obj.anyFunctions();

// ====================================================================================

/**
 * Стрелочный функции не имеют собственного this и наследуют его от родителя (и это круто!)
 */

// const dog = {
//     size: 'small',
//     kind: 'cute',
//     voice: 'bow wow',
//     logThis: () => {
//         console.log(this);
//     }
// };
//
// dog.logThis();

// ==================================

// const speaker = {
//     name: 'Samuel',
//     phrases: [
//         'Nice to meet you.',
//         'Where are you from?',
//         'What do you do?',
//         'What do you like to do (in your free time)?',
//         'What is your phone number?',
//         'Do you have Facebook?',
//     ],
//     startSpeaking (){
//         const loggerCallback = function (item){
//             console.log(`${this.name} says: ${item}`);
//         }
//         this.phrases.forEach(loggerCallback)
//     },
// };
//
// speaker.startSpeaking();

// ====================================================================================
// bind

/**
 * bind() позволяет нам легко выставлять какой именно объект будет привязан к this в момент вызова функции или метода.
 * fun.bind(this)
 */

// const rectangle = {
//     width: 40,
//     height: 50,
//     setPerimeter (){
//         document.querySelector('p.perimeter').innerText =  (this.width + this.height) * 2;
//     },
//     setArea () {
//         document.querySelector('p.area').innerText = this.width * this.height;
//     },
// }

// const perimeterButton = document.querySelector('button.perimeter');
// const areaButton = document.querySelector('button.area');
//
// perimeterButton.addEventListener('click', rectangle.setPerimeter);
// areaButton.addEventListener('click', rectangle.setArea);

// ====================================================================================
// call
/**
 * Метод call() вызывает функцию с указанным значением this и индивидуально предоставленными аргументами.
 * fun.call(this, arg1, arg2, ...args)
 */

// const array = [];
// const collection = document.getElementsByClassName('area');
//
// [].forEach.call(collection, item => console.log(item));
