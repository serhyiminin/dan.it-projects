/**
 * Дана таблица.
 * Сделать так, чтобы по клику на ячейке она окрашивалась в цвет,
 * указанный в её атрибуте data-background-color.
 *
 * У некоторых ячеек этого атрибута нет. Их окрашивать не нужно.
 *
 * Использовать делегирование событий
 * (т.е. не вешать отдельный обработчик на каждый элемент).
 *
 * Дополнительные задания:
 * 1. По клику на следующей ячейке сбрасывать цвет фона у предыдущей кликнутой ячейки.
 * 2.
 */

const table = document.querySelector(`.color-table`);
let pCell = null;

table.addEventListener(`click`, (event) => {
  const color = event.target.getAttribute(`data-background-color`);
  event.target.style.backgroundColor = color;

  if (pCell) {
    pCell.style.backgroundColor = ``;
  }
  pCell = event.target;
});
