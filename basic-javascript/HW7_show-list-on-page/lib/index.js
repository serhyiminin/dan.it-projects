/*
Технические требования:
Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
Каждый из элементов массива вывести на страницу в виде пункта списка;
Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;

Примеры массивов, которые можно выводить на экран:
["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];
Можно взять любой другой массив.

Необязательные задания продвинутой сложности:
Добавьте обработку вложенных массивов. Если внутри массива одним из элементов будет еще один массив, выводить его как вложенный список.
Пример такого массива:
["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать вложенные массивы.

Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.

*/

const placeToInject = document.querySelector(`h1`);
let timeLeft = 3;

const cities = [
  `Kharkiv`,
  `Kyiv`,
  [`Boryspil`, `Irpin`],
  `Odesa`,
  `Lviv`,
  `Dnipro`,
  `Donetsk`,
  `Sevastopol`,
  [`Horishni Plavni`, `Myrhorod`],
];

button.onclick = function () {
  Array.from(document.getElementsByTagName(`ul`)).forEach((elem) =>
    elem.remove()
  );
  showHTMLonPage(cities, placeToInject);
  let beauty = document.querySelector(`ul`);
  beauty.style.listStyle = `square outside none`;

  showCounter();
}; //END of onclick function

function showHTMLonPage(dataArray, location = document.body) {
  let htmlFragment = toHTML(dataArray);
  location.insertAdjacentHTML(`afterend`, htmlFragment);

  function toHTML(array) {
    let list = array
      .map((city) => {
        if (Array.isArray(city)) {
          let list = toHTML(city);
          return list;
        } else {
          return `<li>` + city + `</li>`;
        }
      })
      .join(``);
    return `<ul>` + list + `</ul>`;
  }
}

function showCounter() {
  const counter = document.querySelector(`.btn__label`);
  let div = document.createElement("div");
  div.setAttribute(`id`, `countdown`);
  div.setAttribute(`class`, `countdown`);
  counter.before(div);

  let interval = setInterval(function () {
    if (timeLeft <= 0) {
      clearInterval(interval);
      location += ""; //reload page
    } else {
      document.getElementById(`countdown`).innerHTML =
        `<span>` + timeLeft + `</span>` + ` seconds remaining to clear content`;
    }
    timeLeft -= 1;
  }, 1000);
}
