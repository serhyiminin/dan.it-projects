button.onclick = function () {
  /*
Технические требования:

Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
*/
  const firstNameString = "Tell the student's First name";
  const lastNameString = "Tell the student's Last name";
  const acronym = "MSY";
  let flag = false;
  let err = "";

  const user = createNewUser(acronym);

  if (err !== "ESC pressed or invalid names entered") {
    console.log(user.getLogin());
  } else {
    console.log(err);
  }

  function createNewUser() {
    let firstName;
    let lastName;
    let newUser = {};

    do {
      firstName = getData(firstNameString);

      if (firstName === null) {
        break;
      } else {
        lastName = getData(lastNameString);

        if (lastName === null) {
          break;
        } else {
          flag = true;
        }
      }
    } while (flag === false);

    if (firstName === null || lastName === null) {
      err = "ESC pressed or invalid names entered";
      return;
    }

    Object.defineProperty(newUser, "firstName", {
      writable: false,
      value: firstName,
    });

    Object.defineProperty(newUser, "lastName", {
      writable: false,
      value: lastName,
    });

    Object.defineProperty(newUser, "getLogin", {
      value: function () {
        let login = (this.firstName.charAt(0) + this.lastName).toLowerCase();
        return login;
      },
    });
    return newUser;
  }
}; //END of click function

function getData(string) {
  let data = prompt(string);

  if (
    data === "" ||
    data === null ||
    data === undefined ||
    typeof Number(data) !== "number" ||
    !isNaN(data)
  ) {
    return null;
  } else {
    data = data.trim();
    data = data.replace(/\s+/g, " ");
    return data;
  }
}
