button.onclick = function () {
  /*
Технические требования:

Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.

Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
*/
  const firstNameString = "Tell the student's First name";
  const lastNameString = "Tell the student's Last name";
  const birthdayString = "When you ware born? Enter dd.mm.yyyy";
  let flag = false;
  let err = "";

  const user = createNewUser();

  if (err !== "ESC pressed or invalid input") {
    console.log(user);
    console.log(user.getPassword);
    console.log(user.getAge);
  } else {
    console.log(err);
  }

  function createNewUser() {
    let firstName;
    let lastName;
    let bDay;
    let newUser = {};

    do {
      firstName = getData(firstNameString);

      if (firstName === null) {
        break;
      } else {
        lastName = getData(lastNameString);

        if (lastName === null) {
          break;
        } else {
          bDay = getDate(birthdayString);

          if (bDay === null) {
            break;
          } else {
            flag = true;
          }
        }
      }
    } while (flag === false);

    if (firstName === null || lastName === null || bDay === null) {
      err = "ESC pressed or invalid input";
      return;
    }

    Object.defineProperty(newUser, "_firstName", {
      writable: false,
      value: firstName,
    });

    Object.defineProperty(newUser, "_lastName", {
      writable: false,
      value: lastName,
    });

    Object.defineProperty(newUser, "_birthday", {
      writable: false,
      value: bDay,
    });

    Object.defineProperty(newUser, "getLogin", {
      get() {
        let login = (this._firstName.charAt(0) + this._lastName).toLowerCase();
        return login;
      },
    });

    Object.defineProperty(newUser, "getPassword", {
      get() {
        let pass =
          this._firstName.charAt(0).toUpperCase() +
          this._lastName.toLowerCase() +
          this._birthday.getFullYear();

        if (pass === undefined) {
          return null;
        } else {
          return pass;
        }
      },
    });

    Object.defineProperty(newUser, "getAge", {
      get() {
        let age =
          new Date(Date.now()).getFullYear() - this._birthday.getFullYear();
        return age;
      },
    });

    Object.defineProperty(newUser, "setFirstName", {
      set(name) {
        this._firstName = name;
      },
    });

    Object.defineProperty(newUser, "setLastName", {
      set(surname) {
        this._lastName = surname;
      },
    });

    return newUser;
  }
}; //END of click function

function getData(string) {
  let data = prompt(string);

  if (
    data === "" ||
    data === null ||
    data === undefined ||
    typeof Number(data) !== "number" ||
    !isNaN(data)
  ) {
    return null;
  } else {
    data = data.trim();
    data = data.replace(/\s+/g, " ");
    return data;
  }
}

function getDate(string) {
  let date = prompt(string);
  let d = Date.parse(date);

  if (isNaN(d)) {
    return null;
  } else {
    date = date.trim();
    let dArr = date.split("."); //dd.mm.yyyy
    date = new Date(dArr[2], dArr[1] - 1, dArr[0]);

    return date;
  }
}
