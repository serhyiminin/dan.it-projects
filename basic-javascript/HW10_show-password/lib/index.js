/*Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

В файле index.html лежит разметка для двух полей ввода пароля.
По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения

После нажатия на кнопку страница не должна перезагружаться
Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.*/

const passForm = document.querySelector(`.password-form`);
const button = document.querySelector(`.btn`);
const redAlert = document.querySelector(`.alert`);

passForm.addEventListener(`click`, (e) => {
  let element = e.target;

  if (element.tagName === `I`) {
    element.classList.toggle(`fa-eye-slash`);
  }
  element.type === "password"
    ? (element.type = "text")
    : (element.type = "password");
});

button.addEventListener(`click`, (e) => {
  e.preventDefault();
  let pass = document.querySelectorAll(`input`);

  if (pass[0].value === `` || pass[1].value === ``) {
    redAlert.style.visibility = "hidden";
  } else {
    pass[0].value.match(pass[1].value)
      ? alert(`You are welcome`)
      : (redAlert.style.visibility = "visible");
  }
});
