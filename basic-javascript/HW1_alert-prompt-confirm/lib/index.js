button.onclick = function () {
  let name;
  let age;

  name = getName();
  if (name === "" || name === "undefined") {
    name = getName();
  } else if (name === null) {
    return;
  }
  //At here the name exists with the correct value

  do {
    age = Number(getAge());
  } while (age === "" || isNaN(age) || age === "undefined");

  if (age === 0) {
    alert("You are not allowed to visit this website");
    return;
  } else if (age < 18) {
    alert("You are not allowed to visit this website");
    return;
  } else if (age >= 18 && age <= 22) {
    if (!confirm("Are you sure you want to continue?")) {
      alert("You are not allowed to visit this website");
      return;
    }
  }
  alert(`Welcome ${name}`);
};

function getName() {
  let n = prompt("Назвіть Ваше ім'я");
  return n;
}

function getAge() {
  let a = prompt("Назвіть Ваш вік");
  return a;
}
