//Класна робота Функції

function count(start, end, div) {
  debugger;
  if (arguments.length !== 3) {
    throw new Error("Передайте 3 аргументи");
  }

  switch (true) {
    case start > end:
      console.log("Ошибка! Счёт невозможен.");
      break;
    case start === end:
      console.log("Ошибка! Нечего считать.");
      break;
    default:
      console.log("Отсчёт начат.");
      while (start !== end) {
        if (start % div === 0) {
          console.log(start);
          start = start + 1;
        }
      }
      console.log("Отсчёт завершен.");
      break;
  }
}
// count(5, 10, 2);
// count(5, 5, 1);
// count(2, 1, 1);
