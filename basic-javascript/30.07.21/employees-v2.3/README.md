
Дан массив состоящий из объектов сотрудников. 
````javascript
const employees = [
    {
        name: 'Brandy Hambleton',
        age: 31,
        position: 'designer',
        avatar: 'https://i.pravatar.cc/250?v=1',
        isOnVacation: false,
        id: 132,
    },
    {
        name: 'Killa Shikoba',
        age: 25,
        position: 'QA',
        avatar: 'https://i.pravatar.cc/250?v=2',
        isOnVacation: false,
        tel: '+12244656767',
        id: 12,
    },
    {
        name: 'Colten Walsh',
        age: 46,
        position: 'lead developer',
        avatar: 'https://i.pravatar.cc/250?v=3',
        isOnVacation: false,
        tel: '+443236000887',
        id: 32,
    },
    {
        name: 'Stace Rounds',
        age: 16,
        position: 'intern',
        avatar: 'https://i.pravatar.cc/250?v=4',
        isOnVacation: false,
        id: 11,
    },
    {
        name: 'Joel James',
        age: 21,
        position: 'key developer',
        avatar: 'https://i.pravatar.cc/250?v=5',
        isOnVacation: true,
        tel: '+1888983923',
        id: 54,
    },
    {
        name: 'Brannon Duke',
        age: 22,
        position: 'key developer',
        avatar: 'https://i.pravatar.cc/250?v=6',
        isOnVacation: false,
        tel: '+1878734345888',
        id: 56,
    },
    {
        name: 'Arnav Crouch',
        age: 43,
        position: 'backend developer',
        avatar: 'https://i.pravatar.cc/250?v=7',
        isOnVacation: false,
        tel: '+344654546467',
        id: 516,
    },
    {
        name: 'Regan Bender',
        age: 24,
        position: 'junior developer',
        avatar: 'https://i.pravatar.cc/250?v=8',
        isOnVacation: true,
        id: 116,
    },
    {
        name: 'Esmay Johnston',
        age: 22,
        position: 'QA',
        avatar: 'https://i.pravatar.cc/250?v=9',
        isOnVacation: false,
        tel: '+12676678123',
        id: 74,
    },
];

````

Напишите функцию, которая выведет на экран список карточек сотрудников (в контейнер с id="container") в формате:  

````html
<li>
    <p class="number">1</p>
    <img class="avatar" src="https://avatar-url.com" alt="Jhon Jhonson avatar">
    <p class="name">JHON JHONSON</p>
    <p class="position">designer</p>
    <p class="age">Age: 22</p>
    <p class="id">id: 123</p>
</li>
````

* Список должен быть отсортирован по возрастанию по параметру "id";  
* В списке должны быть только сотрудники, которые не находятся в отпуске;

### Задание 2

* Добавьте обработчики клика на каждую карточку который будет:
  * запрашивать у пользователя в диалоговом окне (prompt) параметр поиска (Имя/Телефон);
  * выводить в модальном окне (alert): 
    * Имя сотрудника если было введено 'имя'
    * Номер телефона сотрудника если было введено 'телефон' и он есть;
    * Сообщение "Номер не найден" если было введено 'телефон' но его нет;
    * Сообщение "Неверный параметр поиска" если было введено что-то кроме 'имя' или 'телефон';
* Добавьте кнопку удаления на каждою карточку
````html
     <button class="delete-button"><img src="assets/delete-icon.png" /></button>
````

### Задание 3
* Добавьте поле поиска и реализуйте поиск по имени
````html
<input id="search" type="text" name="search">
````
* Если ничего не найдено - выведете на экран стилизированное сообщение "Упс... Похоже никого нет ;)"
