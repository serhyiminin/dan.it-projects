/*
Задание
HW1 - ES6-classes
Теоретический вопрос
Объясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

Задание
Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
Создайте геттеры и сеттеры для этих свойств.
Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
Создайте несколько экземпляров объекта Programmer, выведите их в консоль.
*/

/*
Прототипне спадкування полягає в тому, що під час створення об'єкту він успадковує властивості свого прототиту, тобто зв'язок між об'єктом та його прототипом не втрачається навіть за умови ланцюгового створення за зразком нового об'єкту дітей вже від нього. Якщо ми звернемось до властивості об'єкту і він такої властивості мати не буде, то її пошук продовжиться по ланцюгу з середини назовні аж до Object.
*/

class Employee {
  /**
   * Creates an instance of Employee.
   * @param {string} name
   * @param {number} age
   * @param {number} salary
   * @memberof Employee
   */
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  set name(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set age(age) {
    this._age = age;
  }
  get age() {
    return this._age;
  }

  set salary(salary) {
    this._salary = salary;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  /**
   * Creates an instance of Programmer.
   * @param {number} lang
   * @param {string} name inherit from class Employee
   * @param {number} age inherit from class Employee
   * @param {number} salary inherit from class Employee
   * @memberof Programmer
   */
  constructor(lang, ...args) {
    super(...args);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const programmerJunior = new Programmer(1, `John`, 23, 800);
const programmerMiddle = new Programmer(2, `Tom`, 28, 2500);
const programmerSenior = new Programmer(3, `Julia`, 33, 4500);

console.log(programmerJunior);
console.log(programmerMiddle);
console.log(programmerSenior);
