const button = document
  .querySelector("button")
  .addEventListener(`click`, getData);
const container = document.querySelector(".location");
const myIP = document.querySelector(".ip");

async function getData() {
  try {
    await axios
      .get("http://api.ipify.org/?format=json")
      .then(({ status, data }) => {
        if (status === 200) {
          container.innerHTML = "";
          myIP.value = "";
          myIP.value = data.ip;
          const myUrl = `http://ip-api.com/json/` + data.ip;
          try {
            axios.get(myUrl).then(({ status, data }) => {
              if (status === 200) {
                for (let key in data) {
                  if (
                    key !== "status" &&
                    key !== "as" &&
                    key !== "query" &&
                    key !== "org"
                  ) {
                    const listItem = document.createElement(`li`);
                    listItem.innerText = `${key}: ${data[key]}`;
                    container.appendChild(listItem);
                  }
                }
                setTimeout(function () {
                  container.innerHTML = "";
                  myIP.value = "";
                }, 3500);
              }
            });
          } catch (error) {
            console.error(`Location error:`, error);
          }
        }
      });
  } catch (error) {
    console.error(`IP error:`, error);
  }
}
