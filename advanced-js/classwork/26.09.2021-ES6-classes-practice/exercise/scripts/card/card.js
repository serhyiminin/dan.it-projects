/* Classworks */

class Card {
  /**
   * Creates an instance of Card.
   * @param {function} editFunction
   * @param {function} deleteFunction
   * @memberof Card
   */
  constructor(editFunction, deleteFunction) {
    this.editFunction = editFunction;
    this.removeFunction = deleteFunction;
    this.container = document.createElement("div");
    this.contentContainer = document.createElement("div");
    this.editButton = document.createElement("button");
    this.removeButton = document.createElement("button");
  }

  createElements() {
    this.contentContainer.classList.add("card__content-container");

    this.editButton.classList.add("card__btn");
    this.editButton.classList.add("card__edit");
    this.editButton.addEventListener(`click`, this.editFunction.bind(this));

    this.removeButton.classList.add("card__btn");
    this.removeButton.classList.add("card__delete");
    this.removeButton.addEventListener(`click`, this.removeFunction.bind(this));

    this.container.classList.add("card");
    this.container.append(this.editButton);
    this.container.append(this.removeButton);
    this.container.append(this.contentContainer);
  }

  render(selector = "body") {
    this.createElements();
    document.querySelector(selector).append(this.container);
  }
}

export class ArticleCard extends Card {
  /**
   * Creates an instance of ArticleCard.
   * @param {function} editFunction
   * @param {function} deleteFunction
   * @param {string} title
   * @param {string} text
   * @memberof ArticleCard
   */
  constructor(editFunction, deleteFunction, title, text) {
    super(editFunction, deleteFunction);
    this.title = title;
    this.text = text;
    this.titleContainer = document.createElement(`h3`);
    this.textContainer = document.createElement(`p`);
  }

  createElements() {
    super.createElements();
    this.titleContainer.innerHTML = this.title;
    this.textContainer.innerHTML = this.text;
    this.contentContainer.append(this.titleContainer, this.textContainer);
  }
}
