Классы в JavaScript были введены в ECMAScript 2015 и представляют собой синтаксический сахар (_есть несколько отличий, но сейчас они не существенны_) над существующим в JavaScript механизмом прототипного наследования. Синтаксис классов **_не вводит новую объектно-ориентированную модель_**, а предоставляет более простой и понятный способ создания объектов и организации наследования.


## Class declaration & class expression

```js

class SomeClass {}

const AnotherClass = class {};

console.log('SomeClass: ', SomeClass);
console.log('AnotherClass: ', AnotherClass);

```

## Тело класса и задание методов

### Constructor
Метод `constructor` — специальный метод, необходимый для создания и инициализации объектов, созданных, с помощью класса. В классе может быть только один метод с именем `constructor`.

```js

class User {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
}

const jhon = new User('Jhon', 25);

console.log(jhon);
console.log(jhon.name);

```

### Методы

```js

class User {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    
    logInfo(){
        console.log(`Name: ${this.name}; Age: ${this.age}`)
    }
}

const jhon = new User('Jhon', 25);
jhon.logInfo();

```

### Статические методы и свойства

Ключевое слово `static`, определяет статический метод или свойства для класса. Статические методы доступны только самому классу и не доступны объектам созданным с его помощью. 

```js

class User {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }

    static dog = 'Charlie the husky';
    static hasDog(){
        return true;
    }
}

const jhon = new User('Jhon', 25);

console.log('User', User);
console.log('jhon', jhon);

console.log('User.dog', User.dog); // 'Charlie the husky'
console.log('jhon.dog', jhon.dog); // undefined

console.log('User.hasDog', User.hasDog()); // true
console.log('jhon.hasDog', jhon.hasDog()); // TypeError: jhon.hasDog is not a function

```

## get и set
Свойства-аксессоры представлены методами: «геттер» – для чтения и «сеттер» – для записи. Так же как и в объекте они обозначаются get и set:

```js

class User {
    constructor(name, age, password, secretQuestions, secretAnswer) {
        this.name = name;
        this.age = age;
        this._password = password;
        this._secretAnswer = secretAnswer;
        this._secretQuestions = secretQuestions;
    }

    get password() {
        const userSecretAnswer = prompt(this._secretQuestions);
        if (userSecretAnswer === this._secretAnswer) {
            return this._password;
        } else {
            return {
                status: 401,
                message: 'Access denied: wrong password',
            }
        }
    }

    set password(newPassword) {
        const oldPassword = prompt('Enter your current password');
        if (oldPassword === this._password) {
            this._password = newPassword;
            console.log(`Password was successfully changed. New password: ${this._password}`)
            return {
                status: 200,
                message: 'success',
            }
        } else {
            console.error('Access denied: wrong password');
            return {
                status: 401,
                message: 'Access denied: wrong password',
            }
        }
    }
}

const jhon = new User('Jhon', 31, '123123', 'Dog\'s name', 'Bob');

console.log('jhon.password: ', jhon.password);


jhon.password = '321321';

setTimeout(() => {
    jhon.password = '111111';
}, 4000);

```


## Наследование
Ключевое слово `extends` используется в объявлениях классов и выражениях классов для создания класса, дочернего относительно другого класса.

```js

class Child extends Parent {
    ...
}

```

### super

Ключевое слово `super` используется для вызова конструктора родителя в конструкторе потомка. Это очень важный нюанс!

```js
class User {
    constructor(name){
        this.name = name;
    }
}

class Developer extends User {
    /*
    Сейчас в классе Developer не определен собственный конструктор. 
    В таком случае конструткор будет по умолчанию полностью скопирован у родителя.
     */
}
```

```js
class User {
    constructor(name){
        this.name = name;
    }
}

class Developer extends User {
    /*
    Сейчас в классе Developer определен собственный конструктор. 
    В таком случае нам необходимо вызвать конструктор родителя и передать в него все необходимые аргументы.
    Это делается с помощью ключевого слова super.
    */
    constructor(name, salary){
        super(name) // Вызываем родительский конструктор и передаем в него name
        this.salary = salary;
    }
}

```

Мы так же можем использовать синтаксис `rest parameters` чтоб не объявлять аргументы родительского конструктора явно еще раз.

```js

class User {
    constructor(name, age, city, sex){
        this.name = name;
        this.age = age;
        this.city = city;
        this.sex = sex;
    }
}

class Developer extends User {

    constructor(salary, ...args){
        super(...args) // Вызываем родительский конструктор и передаем в него все оставшиеся аргументы
        this.salary = salary;
    }
}

```

### Расширение родительских методов

Ключевое слово `super` так же используется для вызова функций на родителе объекта.

```js

class User {
    constructor(name){
        this.name = name;
    }

    saySomething(){
        console.log(`${this.name} says: Something bla bla bla`);
    }
}

class Developer extends User {
    saySomething() {
        super.saySomething();
        alert('Hey!! Something bla bla bla')
    }
}

const jhon = new User('Jhon');
const sam = new Developer('Sam');

```

## Приватные поля

Один из важнейших принципов объектно-ориентированного программирования – разделение внутреннего и внешнего интерфейсов.


### В объектно-ориентированном программировании свойства и методы разделены на 2 группы:

* Внутренний интерфейс – методы и свойства, доступные из других методов класса, но не снаружи класса.
* Внешний интерфейс – методы и свойства, доступные снаружи класса.

Приватные поля _**не реализованы**_ в JavaScript на уровне языка, но на практике они очень удобны, поэтому их эмулируют с помощью "защищенных свойств".

### Защищённые свойства обычно начинаются с префикса `_`

Это не синтаксис языка: есть хорошо известное соглашение между программистами, что такие свойства и методы не должны быть доступны извне. Большинство программистов следуют этому соглашению.

```js

class User {
    constructor(name){
        this._name = name;
    }

    // Свойство name только для чтения
    get name(){
        return this._name;
    }
}

const jhon = new User('Jhon');

console.log('jhon.name: ', jhon.name);
jhon.name = 'Sam';
console.log('jhon.name: ', jhon.name);

```

## Приватные свойства с помощью `#`

Есть новшество в языке JavaScript, которое почти добавлено в стандарт: оно добавляет поддержку приватных свойств и методов.

Приватные свойства и методы должны начинаться с `#`. Они доступны только внутри класса.

На уровне языка `#` является специальным символом, который означает, что поле приватное. Мы не можем получить к нему доступ извне или из наследуемых классов.

```js

class User {
    constructor(name){
        this.name = name;
    }

    #city = 'Kyiv';

    logInfo(){
        console.log('City: ', this.#city);
        console.log('Name: ', this.name);
    }
}


const jhon = new User('Jhon');

jhon.logInfo(); // City: Kyiv, Name: Jhon

console.log(User.#city); // SyntaxError: Private field '#city' must be declared in an enclosing class

```
