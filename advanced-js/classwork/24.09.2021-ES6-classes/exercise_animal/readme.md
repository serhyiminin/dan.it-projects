# Гонки животных

## Animal
1. Создайте класс `Animal` который будет принимать имя животного.
2. Имя животного должно быть не меньше 3х символов.
3. У животного должен быть метод `render` который принимает селектор контейнера и отрисовывает в этот контейнер html:
```html

<div class="animal">
    <span>{name}</span>
    <button>Move</button>
</div>

```
4. При клике на кнопку "Move" животное должно перемещаться на 20px вправо (создайте метод `move`);

## Dog
1. Создайте класс `Dog` который наследуется от `Animal`.
2. К контейнеру `<div class="animal">` добавьте класс `dog`
3. Добавьте собаке метод `say`, который будет добавлять к контейнеру `<div class="animal">` класс `dog-say` и убирать его через 200мс.
4. Собака гиперактивная и иногда при клике на кнопку "Move" перемещается на 20px дважды, при этом лает (метод `say`). Для рендома используйте функцию `getRandomChance()`

## Cat
1. Создайте класс `Cat` который наследуется от `Animal`.
2. К контейнеру `<div class="animal">` добавьте класс `cat`
3. Добавьте собаке метод `say`, который будет добавлять к контейнеру `<div class="animal">` класс `cat-say` и убирать его через 200мс.
4. Кот не любит когда ему приказывают и иногда при клике на кнопку "Move" не перемещается, при этом мяукает (метод `say`). Для рендома используйте функцию `getRandomChance()`

## Snake
1. Создайте класс `Snake` который наследуется от `Animal`.
2. Змея принимает дополнительные параметр `type`. Это параметр может принимать только два значения (`'poison'` или `'safe'`);
3. К контейнеру `<div class="animal">` добавьте класс `snake`
4. Змея очень медленная и при клике на кнопку "Move" перемещается всего-лишь на 5px. Также змея имеет шанс зашипеть (вывести на экран `alert` с текстом 'Sssssssssssssssssss!!!'). Для рендома используйте функцию `getRandomChance()`;
5. Если змея ядовитая, то после того как она шипит все остальные животные разбегаются (удалить с экрана всех кошек и собак).

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

```js

class Animal {
    /**
     * @param name {string}
     */
    constructor(name) {
        if (typeof name !== 'string') throw new Error('Name should be a string');
        if (name.length < 3) throw new Error('Name should include min 3 characters');
        this._name = name;
        this.x = 0;
        this.animalElement = document.createElement('div');
        this.moveButton = document.createElement('button');
    }

    set name(name){
        if (typeof name !== 'string') throw new Error('Name should be a string');
        if (name.length < 3) throw new Error('Name should include min 3 characters');
        this._name = name;
    }

    get name(){
        return this._name;
    }

    getRandomChance() {
        return Math.random() < 0.5;
    }

    move(){
        this.x += 20;
        this.animalElement.style.transform = `translateX(${this.x}px)`;
    }

    render(selector){
        this.animalElement.classList.add('animal');
        this.animalElement.innerHTML = `<span class="name">${this._name}</span>`;
        this.moveButton.innerHTML = 'Move';
        this.moveButton.addEventListener('click', this.move.bind(this));
        this.animalElement.append(this.moveButton);
        document.querySelector(selector).append(this.animalElement);
    }
}

class Dog extends Animal {
    /**
     * @param name {string}
     */
    constructor(name) {
        super(name);
    }

    render(selector){
        super.render(selector);
        this.animalElement.classList.add('dog');
    }

    say(){
        this.animalElement.classList.add('dog-say');
        setTimeout(() => {
            this.animalElement.classList.remove('dog-say');
        }, 200);
    }

    move(){
        super.move();
        if (this.getRandomChance()) {
            super.move();
            this.say();
        }
    }
}

class Cat extends Animal {
    /**
     * @param name {string}
     */
    constructor(name) {
        super(name);
    }

    render(selector){
        super.render(selector);
        this.animalElement.classList.add('cat');
    }

    say(){
        this.animalElement.classList.add('cat-say');
        setTimeout(() => {
            this.animalElement.classList.remove('cat-say');
        }, 200)
    }

    move(){
        if (this.getRandomChance()) {
            super.move();
        } else {
            this.say();
        }
    }
}

class Snake extends Animal {
    /**
     * @param name {string}
     * @param type {'poison'|'safe'}
     */
    constructor(name, type = 'safe') {
        super(name);
        if (type !== 'poison' && type !== 'safe') throw new Error('Type of snake should be one of "poison" or "safe" ');
        this._type = type;
    }

    get type(){
        return this._type;
    }

    render(selector){
        super.render(selector);
        this.animalElement.classList.add('snake');
    }

    move(){
        this.x -= 15;
        super.move();
        if (this.getRandomChance()) {
            alert('Sssssssssssssssssssss!!!')
            if (this._type === 'poison') {
                document.querySelectorAll('.dog').forEach(elem => {
                    elem.remove();
                })

                document.querySelectorAll('.cat').forEach(elem => {
                    elem.remove();
                })
            }
        }
    }
}


const dog = new Dog('Bob');
const cat = new Cat('Cat');
const snake = new Snake('Poison Simon', 'poison');
const snake2 = new Snake('Safe Simon');

dog.render('.container');
cat.render('.container');
snake.render('.container');
snake2.render('.container');

```
