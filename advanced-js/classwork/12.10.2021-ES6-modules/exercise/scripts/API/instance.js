const config = {
  baseURL: `https://ajax.test-danit.com/api/json/posts`,
};

const instance = axios.create(config);

export default instance;
