let counter = 0

window.addEventListener('scroll', () => {
    console.log('window.scrollY', window.scrollY);
    counter++;
    console.log('counter: ', counter);

    document.querySelectorAll('h3').forEach(({ style }) => {
        style.transition = '.3s ease-uot'
        if (!style.color) {
            style.color = 'blue';
            style.transform = 'translate(5px, 0)';
        } else if (style.color === 'blue') {
            style.color = 'green';
            style.transform = 'translate(-5px, 0)';
        } else if (style.color === 'green') {
            style.color = 'yellow';
            style.transform = 'translate(0, 5px)';
        } else if (style.color === 'yellow') {
            style.color = 'tomato';
            style.transform = 'translate(0, -5px)';
        } else if (style.color === 'tomato') {
            style.color = '';
            style.transform = 'translate(0, 0)';
        }
    })
});
