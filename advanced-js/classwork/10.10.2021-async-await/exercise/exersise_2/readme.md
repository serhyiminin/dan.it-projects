[API](https://rapidapi.com/malaaddincelik/api/fitness-calculator/)

Mets

Получить список активностей и отрисовать в виде `<option>` в `<select name="activity" id="activity">`

По клику на `<button>Get Activity Calories</button>` рассчитывать и выводить на экран количество потраченных калорий;
```js

const options = {
    method: 'GET',
    url: 'https://fitness-calculator.p.rapidapi.com/mets',
    headers: {
        'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
        'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
    }
};

axios.request(options).then(function (response) {
    console.log(response.data);
}).catch(function (error) {
    console.error(error);
});

```

Ideal weight

```js

const options = {
  method: 'GET',
  url: 'https://fitness-calculator.p.rapidapi.com/idealweight',
  params: {gender: 'male', weight: '70', height: '178'},
  headers: {
    'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
    'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
  }
};

axios.request(options).then(function (response) {
	console.log(response.data);
}).catch(function (error) {
	console.error(error);
});

```

Bmi

```js

const options = {
  method: 'GET',
  url: 'https://fitness-calculator.p.rapidapi.com/bmi',
  params: {age: '25', weight: '65', height: '180'},
  headers: {
    'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
    'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
  }
};

axios.request(options).then(function (response) {
	console.log(response.data);
}).catch(function (error) {
	console.error(error);
});

```
