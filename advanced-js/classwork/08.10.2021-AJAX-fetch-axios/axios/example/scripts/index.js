
axios.interceptors.request.use(config => {
    // Сделать что-то до того как запрос будет отправлен
    config.headers['Authorisation'] = 'Basic YWxhZGRpbjpvcGVuc2VzYW1l';
    return config;
}, error => {
    // Сделать что-то c ошибкой отправки запроса
    return Promise.reject(error);
});

axios.interceptors.response.use(response => {
    // Сделать что-то до того как reponse попадет в then
    console.log('===================================================');
    console.log('RESPONSE', response);
    console.log('===================================================');

    return response;
}, error => {
    // Сделать что-то c ошибкой отправки запроса
    return Promise.reject(error);
});

axios.get('https://ajax.test-danit.com/api/json/users').then(({ data }) => console.log(data));
axios.get('https://ajax.test-danit.com/api/json/posts ').then(({ data }) => console.log(data));
axios.get('https://ajax.test-danit.com/api/json/comments').then(({ data }) => console.log(data));
axios.get('https://ajax.test-danit.com/api/json/photos ').then(({ data }) => console.log(data));

