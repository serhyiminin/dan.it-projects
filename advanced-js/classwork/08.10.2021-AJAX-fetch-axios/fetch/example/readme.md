# Fetch
Fetch API предоставляет интерфейс для получения ресурсов (в том числе по сети). Он покажется знакомым любому, кто использовал XMLHttpRequest, но новый API является более мощным и гибким набором функций.

[Fetch polyfill](https://github.com/github/fetch/blob/master/fetch.js)


## Базовый синтаксис

```js

let promise = fetch(url, [options])

```

* `url` – URL для отправки запроса.
* `options` – дополнительные параметры: метод, заголовки и так далее.


Без options это простой GET-запрос, скачивающий содержимое по адресу url.

## Получение ответа

Обычно происходит в два этапа

<blockquote>
Во-первых, promise выполняется с объектом встроенного класса Response в качестве результата, как только сервер пришлёт заголовки ответа.
</blockquote>

## Response

```js

fetch('https://ajax.test-danit.com/api/json/users').then(response => console.log(response))

```

<hr />

### [HTTP status](https://developer.mozilla.org/ru/docs/Web/HTTP/Status)

* `status` – код статуса HTTP-запроса, например 200.
* `ok` – логическое значение: будет true, если код HTTP-статуса в диапазоне 200-299.

```js

fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => {
        const { status, ok } = response;
        console.log(status);
        console.log(ok);
        
        if (ok) {
            return response.json();
        } else {
            throw new Error(`Bad status: ${status}`)
        }
    })
    .then(data => console.log(data))
    .catch(error => console.error(error));

```
<hr />

###  Доступ к телу ответа 

`Response` предоставляет несколько методов, основанных на промисах, для доступа к телу ответа в различных форматах:

* `response.text()` – читает ответ и возвращает как обычный текст,
* `response.json()` – декодирует ответ в формате JSON,
* `response.formData()` – возвращает ответ как объект FormData,
* `response.blob()` – возвращает объект как Blob (бинарные данные с типом),
* `response.arrayBuffer()` – возвращает ответ как ArrayBuffer (низкоуровневое представление бинарных данных),

```js

fetch('https://ajax.test-danit.com/api/json/users')
  .then(response => response.json())
  .then(users => users.forEach(user => document.write(user.name)));

```

```js

fetch('https://st3.depositphotos.com/1000911/19213/i/1600/depositphotos_192134550-stock-photo-welsh-corgy-pembroke.jpg')
    .then(response => response.blob())
    .then(data => console.log(data));

```

<hr />

### Заголовки ответа

Заголовки ответа хранятся в похожем на [Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map) объекте `response.headers`.

```js

fetch('https://ajax.test-danit.com/api/json/users')
    .then(({ headers }) => {
        console.log(headers);
        console.log(headers.get('content-type'));
    });

```
<hr />

## Опции

### Method

Устанавливает [метод HTTP запроса](https://developer.mozilla.org/ru/docs/Web/HTTP/Methods)

### Body

Устанавливает тело запроса. Может быть одним из:

* строка (например, в формате `JSON`),
* объект `FormData` для отправки данных как `form/multipart`,
* `Blob/BufferSource` для отправки бинарных данных,
* `URLSearchParams` для отправки данных в кодировке `x-www-form-urlencoded`, используется редко.

### Headers, [Заголовки запроса](https://developer.mozilla.org/ru/docs/Web/HTTP/Headers)

Для установки заголовка запроса в fetch мы можем использовать опцию headers. Она содержит объект с исходящими заголовками, например:

```js

fetch('https://ajax.test-danit.com/api/json/posts', {
    method: 'POST',
    body: JSON.stringify({
        title: 'Post',
        body: 'SomePost',
    }),
    headers: {
        'Content-Type': 'application/json;charset=utf-8',
    },
})
    .then(response => response.json())
    .then(data => console.log(data));

```
