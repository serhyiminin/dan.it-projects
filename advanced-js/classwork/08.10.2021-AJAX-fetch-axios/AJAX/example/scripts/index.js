// 1. Создаем XMLHttpRequest
const xhr = new XMLHttpRequest();

// 2. Инициализируем запрос
xhr.open('GET', 'https://casdftfact.ninja/fact');
console.log(xhr)

// 3. Послать запрос.
xhr.send();


xhr.onload = () => {
    console.log(xhr);
    if (xhr.status === 200) {
        console.log(JSON.parse(xhr.response));
    } else {
        console.error(`Bad request! Status: ${xhr.status}`);
    }
};

xhr.onprogress = (event) => {
    // event.loaded - количество загруженных байт
    // event.lengthComputable = равно true, если сервер присылает заголовок Content-Length
    // event.total - количество байт всего (только если lengthComputable равно true)
    console.log(`Загружено ${event.loaded} из ${event.total}`);
};

xhr.onerror = function() {
    console.error('Something goes wrong');
};
