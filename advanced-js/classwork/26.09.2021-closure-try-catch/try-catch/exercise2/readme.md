# Палитра цветов
<hr>

Дан массив цветов в формате HEX

Выведите на экран палитру состоящую из:

```html
<div class="item">
    <span>#000000</span>
</div>
```

где `#000000` это код цвета.

Если код цвета не соответствует формату HEX - выведет в консоль ошибку и не отрисовывайте блок в палитру;

P.S. Используйте `try...catch`, свой тип ошибки наследуемый от `Error`, все остальные ошибки пробросьте дальше.
