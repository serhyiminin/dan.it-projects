/*
Напишите функцию changeTextSize, у которой будут такие аргументы:
1. Ссылка на DOM-элемент, размер текста которого нужно изменить без регистрации и sms.
2. Величина в px, на которую нужно изменить текст,  возвращает функцию, меняющую размер текста на заданную величину.

С помощью этой функции создайте две:
- одна увеличивает текст на 2px от изначального;
- вторая - уменьшает на 3px.

После чего повесьте полученные функции в качестве  обработчиков на кнопки с id="increase-text" и id="decrease-text".

 */
