const users = require('../../bdMock/users');
const emailRegExp = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
let id = 1;

const register = (app) => {
    app.post('/register', (req, res) => {

        if(req.headers && req.headers['content-type']) {
            if (!(/application\/json/gi.test(req.headers['content-type']))){
                return res.status(418).send({
                    status: `error`,
                    error: `Wrong Content-type header: expected 'application/json' instead of '${req.headers['content-type']}'`
                })
            }
        }


        const { name, age, city, email, password, repeatPassword } = req.body;

        if (!email) {
            return res.status(400).send({
                status: `error`,
                error: `Email is required`
            })
        }

        if (!password || !repeatPassword) {
            return res.status(400).send({
                status: `error`,
                error: `Password and repeatPassword is required`
            })
        }

        if (!emailRegExp.test(email)) {
            return res.status(400).send({
                status: `error`,
                error: `Invalid format of Email`
            })
        }

        if (password !== repeatPassword) {
            return res.status(400).send({
                status: `error`,
                error: `Passwords don't match`
            })
        }

        const user = { name, age, city, email, password, id, avatar: 'https://i.pravatar.cc/300', toDoList: [] };
        users.push(user);
        const { password: p, ...rest } = user;
        id++;
        return res.status(200).send(JSON.stringify({
            status: 'success',
            message: 'User have successfully registered',
            user: {...rest},
        }))
    })
}

module.exports = register;
