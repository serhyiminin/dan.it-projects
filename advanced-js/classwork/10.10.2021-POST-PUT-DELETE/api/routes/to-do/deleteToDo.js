const users = require('../../bdMock/users');
const querystring = require('querystring');
const authToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZXNzYWdlIjoiSldUIFJ1bGVzISIsImlhdCI6MTQ1OTQ0ODExOSwiZXhwIjoxNDU5NDU0NTE5fQ.-yIVBD5b73C75osbmwwshQNRC7frWUYrqaTjTpza2y4';

const deleteToDo = (app) => {
    app.delete('/to-do', (req, res) => {

        if (req.headers && !req.headers['authorization']) {
            return res.status(401).send({
                status: `error`,
                error: `Unauthorized: Wrong 'Authorization' header type: expected 'Token <tokenValue>' instead of '${req.headers['authorization']}'`
            })
        }

        if(req.headers && req.headers['authorization']) {
            const [type, token] = req.headers['authorization'].split(' ');

            if (type.toLowerCase() !== 'token'){
                return res.status(401).send({
                    status: `error`,
                    error: `Unauthorized: Wrong 'Authorization' header type: expected 'Token' instead of '${type}'`
                })
            }

            if (token !== authToken){
                return res.status(401).send({
                    status: `error`,
                    error: `Unauthorized: Wrong 'Authorization' header token: '${token}'`
                })
            }
        }

        const { toDoId, userId } = req.query;

        if (!toDoId) {
            return res.status(400).send({
                status: `error`,
                error: `toDoId is required`
            })
        }

        if (!userId) {
            return res.status(400).send({
                status: `error`,
                error: `userId is required`
            })
        }

        const index = users.findIndex(({ id }) => +id === +userId);

        if (index < 0){
            return res.status(404).send({
                status: `error`,
                error: `User with id '${userId}' not found`,
            })
        }



        if (users[index] && users[index].toDoList) {
            const toDoIndex = users[index].toDoList.findIndex(({ id }) => id === toDoId);
            if (toDoIndex < 0){
                return res.status(404).send({
                    status: `error`,
                    error: `ToDo item with id '${toDoId}' not found`,
                })
            }

            users[index].toDoList.splice(toDoIndex, 1);
        }

        return res.status(200).send(JSON.stringify({
            status: 'success',
            user: {
                ...users[index]
            },
        }))
    })
}

module.exports = deleteToDo;
