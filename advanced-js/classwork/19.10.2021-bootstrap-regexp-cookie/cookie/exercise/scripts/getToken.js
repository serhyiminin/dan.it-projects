export const getToken = () => {
  const result = document.cookie.split("; ");
  const token = result.find((e) => e.includes("token"));
  return token.split("=")[1];
};
