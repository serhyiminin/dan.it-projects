import { setToken } from "./setToken.js";
import { getToken } from "./getToken.js";
import { removeToken } from "./removeToken.js";

setToken();
console.log(getToken("token"));
removeToken("token");
