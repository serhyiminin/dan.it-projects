export function removeToken(name) {
  setCookie(name, "", {
    "max-age": -1,
  });
}
