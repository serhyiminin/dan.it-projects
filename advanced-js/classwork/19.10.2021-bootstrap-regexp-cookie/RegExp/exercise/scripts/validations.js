export const nameValidation = ({ target }) => {
  const regExp = / [^a-zA-Z\s]/g;
  const newValue = target.value.replace(regExp, "");
  target.value = newValue;
};

export const commentsValidation = ({ target }) => {
  const regExp = /<.*>/g;
  const newValue = target.value.replace(regExp, "");
  target.value = newValue;
};
