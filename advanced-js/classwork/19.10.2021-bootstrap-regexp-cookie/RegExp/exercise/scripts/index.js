import setTogglePassword from "./togglePassword.js";
import { nameValidation, commentsValidation } from "./validations.js";

setTogglePassword();
document.querySelector(".").addEventListener("input", nameValidation);
document.querySelector(".comment").addEventListener("blure", commentsValidation);
