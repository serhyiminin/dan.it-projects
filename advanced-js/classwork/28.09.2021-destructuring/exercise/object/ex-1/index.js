/*
1. Выведите в параграф с id = href строку url текущей страницы в браузере;
2. Напишите функционал перезагрузки страницы по клику на кнопку с id = reload;
 */

const {
  location: { href, reload },
} = window;
console.log(reload);

const reload = window.location.reload;

const paragraph = document.querySelector(`#href`);
paragraph.innerHTML = href;
const reloadBtn = document.querySelector(`#reload`);
reloadBtn.addEventListener(`click`, reload.bind(window.location));
