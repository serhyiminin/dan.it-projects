/*
Напишите функцию getPatientStatus, которая принимает в качестве аргумент рост в см и вес в кг пациента, и возвращает 2 параметра - индекс массы тела и степень ожирения. Степени ожирения при разном значении индекса массы тела:
- от 10 до 15 - анорексия;
- от 16 до 25 - норма;
- от 26 до 30 - лишний вес;
- от 31 до 35 - I степень;
- от 36 до 40 - II степень;
- от 41 и выше - III степень;

 Используйте лучшие правила создания модульного кода
 */

const getPatientStatus = (height, weight) => {
  const BMI = Math.floor(weight / (((height / 100) * height) / 100));
  const obesityDegree;
  switch (BMI) {
     case BMI>=10 && BMI<=15:
      obesityDegree = `анорексия`;
        break;
        case BMI>=16 && BMI<=25:
      obesityDegree = `норма`;
        break;
        case BMI>=26 && BMI<=30:
      obesityDegree = `лишний вес`;
        break;
        case BMI>=31 && BMI<=35:
      obesityDegree = `I степень`;
        break;
        case BMI>=36 && BMI<=40:
      obesityDegree = `II степень`;
        break;
        case BMI>=41:
         obesityDegree = `III степень`;
           break;
     default:
      obesityDegree = `для дітей не обраховується`
        break;
  }
  return [BMI, obesityDegree];
};

console.log(getPatientStatus(189, 112));
