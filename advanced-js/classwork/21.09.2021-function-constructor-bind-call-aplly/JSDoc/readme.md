![](assets/1.jpg)

## «Хороший код — самодокументированный код»

Плохо: 
```js

/*
Устанавливает значение 32 для переменной age
*/
let age = 32;

```

Хорошо: 
```js

function addSetEntry(set, value) {   
  /* 
   Не возвращать `set.add`, потому что эта цепочка вызовов не сработает в IE 11.
  */  
  set.add(value);    
  return set;  
}

```

#### Постоянно представляйте себе как будто посторонний разработчик читает ваш код. Если какие-то вещи покажутся ему неочевидными или трудно понимаемыми - добавьте комментарий. Это считается правилом хорошего тона и позволит вам сэкономить свое время в дальнейшем.


# [JSDoc](https://jsdoc.app/)
