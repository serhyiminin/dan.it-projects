const someText =
  'Модель наследования в JavaScript может озадачить опытных разработчиков на высокоуровневых объектно-ориентированных языках (таких, например, как Java или C++), поскольку она динамическая и не включает в себя реализацию понятия class (хотя ключевое слово class, бывшее долгие годы зарезервированным, и приобрело практическое значение в стандарте ES2015, однако, Class в JavaScript ES>=6 представляет собой лишь "синтаксический сахар" поверх прототипно-ориентированной модели наследования).\n' +
  "\n" +
  "В плане наследования JavaScript работает лишь с одной сущностью: объектами. Каждый объект имеет внутреннюю ссылку на другой объект, называемый его прототипом. У объекта-прототипа также есть свой собственный прототип и так далее до тех пор, пока цепочка не завершится объектом, у которого свойство prototype равно null.  По определению, null не имеет прототипа и является завершающим звеном в цепочке прототипов.\n" +
  "\n" +
  "Хотя прототипную модель наследования некоторые относят к недостаткам JavaScript, на самом деле она мощнее классической. К примеру, поверх неё можно предельно просто реализовать классическое наследование, а вот попытки совершить обратное непременно вынудят вас попотеть.";

function ShowMore(text, lineNumber, selector) {
  this.text = text;
  this.lineNumber = lineNumber;
  this.selector = document.querySelector(selector);
  this.span = document.createElement(`span`);
  this.container = document.createElement(`div`);
  this.button = document.createElement(`button`);
}

ShowMore.prototype.render = function () {
  this.span.innerHTML = this.text;
  this.span.classList.add("show-more__text");
  this.span.style[`-webkit-line-clamp`] = this.lineNumber;

  this.container.classList.add("show-more__wrapper");
  this.button.classList.add("show-more__button");
  this.button.innerHTML = "Show more";

  this.container.append(this.span);
  this.container.append(this.button);
  this.selector.append(this.container);

  this.button.addEventListener(`click`, () => {
    if (this.span.style.display === "block") {
      this.span.style.display = `-webkit-box`;
      this.button.innerHTML = "Show More";
    } else {
      this.span.style.display = "block";
      this.button.innerHTML = "Show Less";
    }
  });
};

const newShowMore = new ShowMore(someText, 3, ".container");
newShowMore.render();
