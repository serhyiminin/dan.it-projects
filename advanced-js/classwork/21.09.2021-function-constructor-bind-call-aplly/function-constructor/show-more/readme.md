Функция конструктор ShowMore должна принимать в себя текст, количество отображаемых строк и селектор главного контейнера.

В функции должен быть метод `render` который должен будет отрисовывать в полученный контейнер 

```html
<div class="show-more__wrapper">
    <span class="show-more__text">Some Text</span>
    <button class="show-more__button">Show more</button>
</div>
```

На `<span class="show-more__text">` при инициализации должен добавляться стиль 
```css
-webkit-line-clamp: ${NumberOfLine};
```

где `${NumberOfLine}` - аргумент функции конструктора (количество отображаемых строк);

При клике на кнопку "Show more" блок должен раскрываться (свойство стиля `display` должно становиться `block`) а текст кнопки должен меняться на `Show less`. При повторном клике все должно возвращаться в первоначальное состояние.

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

Решение
```js
/**
 * @param text {string} - text included in show more
 * @param lines {number} - number of showed lines
 * @param containerSelector {string} - main container selector (in which show more will append)
 */
function ShowMore(text, lines, containerSelector){
    this.text = text;
    this.lines = lines;
    this.container = document.querySelector(containerSelector);
    this.showMoreButton = document.createElement('button');
    this.elemWrapper = document.createElement('div');
    this.elem = document.createElement('span');
}

ShowMore.prototype.createElem = function (){
    this.elem.innerHTML = this.text;
    this.elem.classList.add('show-more__text');
    this.elem.style['-webkit-line-clamp'] = this.lines;
}

ShowMore.prototype.createButton = function(){
    this.showMoreButton.innerHTML = 'Show more';
    this.showMoreButton.classList.add('show-more__button');
    this.showMoreButton.addEventListener('click',() => {
        if (this.elem.style.display === 'block') {
            this.elem.style.display = '';
            this.showMoreButton.innerHTML = 'Show more';
        } else {
            this.elem.style.display = 'block';
            this.showMoreButton.innerHTML = 'Show less';
        }
    });
}

ShowMore.prototype.createWrapper = function(){
    this.elemWrapper.append(this.elem);
    this.elemWrapper.append(this.showMoreButton);
    this.elemWrapper.classList.add('show-more__wrapper');
}

ShowMore.prototype.render = function(){
    this.createElem();
    this.createButton();
    this.createWrapper();
    this.container.append(this.elemWrapper);
}

```
