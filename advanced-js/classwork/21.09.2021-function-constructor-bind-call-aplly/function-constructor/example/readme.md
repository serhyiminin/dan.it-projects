Обычный синтаксис `{...}` позволяет создать только один объект. Но зачастую нам нужно создать множество однотипных объектов, таких как пользователи, элементы меню и т.д.

Это можно сделать при помощи функции-конструктора и оператора `new`.

# Функции-конструктора

Функции-конструкторы являются обычными функциями. Но есть два соглашения:

1. Имя функции-конструктора должно начинаться с большой буквы.
2. Функция-конструктор должна вызываться при помощи оператора ```new```.

# ```new Function(...)```

Когда функция вызывается как ```new Function(...)```, происходит следующее:

* Создаётся новый пустой объект, и он присваивается this.
* Выполняется код функции. Обычно он модифицирует this, добавляет туда новые свойства.
* Возвращается значение this.

```js

function Employer(name, role, salary){
    this.name = name;
    this.role = role;
    this.salary = salary;
    this.log = function(){
        console.log(`Name: ${this.name}, Role: ${this.role}, Salary: ${this.salary}`);
    }
};

```

## `return`

Обычно конструкторы ничего не возвращают явно. Их задача – записать все необходимое в this, который в итоге станет результатом. То-есть можно считать что по умолчанию конструктор возвращает объект ```this```.

Но если `return` всё же есть, то применяется простое правило:

* При вызове return с объектом, будет возвращён объект, а не ```this```.
* При вызове return с примитивным значением, примитивное значение будет отброшено.

## Стрелочные функции
Стрелочные функции не могут быть вызваны через оператор `new` и не используются в качестве конструктора.


# Прототипы
Прототипы - это механизм, с помощью которого объекты JavaScript наследуют свойства друг от друга.

JavaScript часто описывают как язык прототипного наследования — каждый объект, имеет объект-прототип, который выступает как шаблон, от которого объект наследует методы и свойства. 

Объект-прототип так же может иметь свой прототип и наследовать его свойства и методы и так далее. Это часто называется цепочкой прототипов и объясняет почему одним объектам доступны свойства и методы которые определены в других объектах.

```js

const object = {
    name: 'someObject',
    sayName(){
        alert(this.name);
    }
}
console.log(object)
console.log(object.hasOwnProperty('name')); // метод hasOwnProperty вызывается из прототипа

```

## Свойство prototype
Специальное свойство объекта, в котором хранится прототип.

Доступ к нему можно получить через устаревший синтаксис обращаясь к свойству  `__proto__`:

```js

function Student(name){
    this.name = name;
}

const jhon = new Student('Jhon');

console.log(jhon);
console.log(jhon.__proto__);

```

Не используйте свойство  `__proto__`!

Современный синтаксис - методы `Object.getPrototypeOf(obj)` и `Object.setPrototypeOf(obj, proto)`;

```js

function Student(name){
    this.name = name;
}

const jhon = new Student('Jhon');

console.log(jhon);
console.log(Object.getPrototypeOf(jhon));

```

### Создание свойств и методов в prototype
Свойства и методы можно добавлять в прототип с помощью привычного синтаксиса через `.`: `Fenction.prototype.someMethod = function(){...}`

```js

function Student(name){
    this.name = name;
}

Student.prototype.alertName = function(){alert(this.name)};

const jhon = new Student('Jhon');

console.log(jhon);
console.log(Object.getPrototypeOf(jhon));

jhon.alertName();

```

### Почему prototype?

```js

function Student(name){
    this.name = name;
    this.alertFromObject = function(){alert(this.name)};
}

Student.prototype.alertFromPrototype = function(){alert(this.name)};

const jhon = new Student('Jhon');
const sam = new Student('Sam');

console.log(jhon);
console.log(sam);

console.log('From object: ', jhon.alertFromObject === sam.alertFromObject);
console.log('From prototype: ', jhon.alertFromPrototype === sam.alertFromPrototype);

```

### Свойство constructor
Свойство `constructor` объекта `prototype ` указывает на исходную функцию-конструктор.

```js

function Student(name){
    this.name = name;
}

const jhon = new Student('Jhon');

console.log(jhon);
console.log(jhon.constructor);

```
