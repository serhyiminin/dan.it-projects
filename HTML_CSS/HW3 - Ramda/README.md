# HW3 - Ramda

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Contributing](#contributing)

## About <a name = "about"></a>

It is my third homework for DAN.IT.

## Getting Started <a name = "getting_started"></a>

The task was to code a simple html page with CSS display properties according to the conditions provided.

### Prerequisites

Any browser fits you, I prefer Chrome

### Installing

git clone https://gitlab.com/serhyi.minin/dan.it-projects.git

## Usage <a name = "usage"></a>

Any suggestions are welcomed!

## Contributing <a name = "contributing"></a>

DAN.IT Education
