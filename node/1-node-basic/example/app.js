const colors = require('colors');
const fs = require('fs');
const http = require('http');
const { resolve } = require('path');

// fs.readFile('./files/hello.txt', 'utf8',(error, data) => {
//     if (error) {
//         console.log(error);
//     }
//
//     fs.writeFile('./files/add/hello-copy.txt', `COPY: ${data}`, (error) => {
//         if (error) {
//             console.log(error);
//         }
//     })
//     console.log(data);
// })

// fs.mkdir('test/add/files/test', { recursive: true }, (error) => {
//     if (error) {
//         console.log(error);
//     }
// })

// fs.unlink('./files/hello-copy.txt', (err) => console.log(err));
// fs.rm('test', {recursive: true }, (err) => console.log(err));
// const data = fs.readFileSync('./files/hello.txt', 'utf8');
// console.log(data);

// console.log(fs.existsSync('files'))


// const myPath = path.resolve(__dirname, 'files/add', 'hello-copy.txt');
//
// console.log(myPath);

const PORT = 3000;

const server = http.createServer((req, res) => {
    // res.statusCode = 200;
    // res.setHeader('Content-Type', 'text/html');
    // res.setHeader('Stanislav', '123');

    res.writeHead('200', {'Content-Type': 'text/html'})
    res.write('<h1>HELLO WORLD</h1>');
    res.write('<p>HELLO P</p>');
    res.write('<h2>HELLO H2</h2>');
    res.end();
});

server.listen(PORT, 'localhost', () => {
    console.log(`Server listen on port: ${PORT}`)
})
