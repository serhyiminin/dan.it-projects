import express from 'express';
import routes from "./routes/index.js";
import bodyParser from "body-parser";

const app = express();
const port = 3000;

app.use(bodyParser.json())

routes(app);

app.listen(port, () => console.log(`server started at port ${port}`));