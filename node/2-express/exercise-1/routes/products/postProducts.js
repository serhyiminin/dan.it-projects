import products from "../../products.js";
import {sendBadResponse, sendGoodResponse} from "../../utils/senfFunction.js";

const postProducts = (app) => {
    app.post("/products", (req, res)=>{
        if(!req.body.name || !req.body.price){
            // const body ={
            //     status:"error",
            //     data:"Name or Price required"
            // }
            // res.status(400).json(body);

            sendBadResponse(res, 400, "Name or Price required");
        }

        products.push(req.body);
        // const body ={
        //     status:"success",
        //     data:products
        // }
        // // res.status(200).send(body);
        // res.status(200).json(body);

        sendGoodResponse(res, products);
    })
}

export default postProducts;