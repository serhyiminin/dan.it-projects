import products from "../../products.js";
import {sendGoodResponse} from "../../utils/senfFunction.js";

const getProducts = (app) => {
    app.get("/products", (req, res) => {
        // const body = {
        //     status: "success",
        //     data: products
        // }
        // // res.status(200).send(body);
        // res.status(200).json(body);

        sendGoodResponse(res, products);
    })
}

export default getProducts;