import getProducts from "./products/getProducts.js";
import postProducts from "./products/postProducts.js";

const routes = (app) => {
    getProducts(app);
    postProducts(app);
}

export default routes;