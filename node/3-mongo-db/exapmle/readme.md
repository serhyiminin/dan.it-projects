## [MongoDB](https://www.mongodb.com/)
## [Mongoose](https://mongoosejs.com/)

<hr />

### [Быстрый старт](https://mongoosejs.com/docs/index.html)
#### [Получение всех данных](https://mongoosejs.com/docs/api/model.html#model_Model.find) - `Model.find()`;
#### [Получение элемента коллекции по id ](https://mongoosejs.com/docs/api/model.html#model_Model.findById) - `Model.findById()`
#### [Получение элемента коллекции по параметрам](https://mongoosejs.com/docs/api/model.html#model_Model.findOne) - `Model.findOne()`
#### [Добавление нового элемента](https://mongoosejs.com/docs/api.html#model_Model-save) - `new Model.save()`;
#### [Редактирование](https://mongoosejs.com/docs/api/model.html#model_Model.findByIdAndUpdate) - `Model.findByIdAndUpdate()`
#### [Удаление](https://mongoosejs.com/docs/api/model.html#model_Model.findByIdAndDelete) - `Model.findByIdAndDelete()`

<hr />

##### [timestamp](https://mongoosejs.com/docs/guide.html#timestamps)
##### [dotenv](https://www.npmjs.com/package/dotenv)
##### [chalk](https://www.npmjs.com/package/chalk)

