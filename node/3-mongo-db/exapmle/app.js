import express from 'express';
import mongoose from "mongoose";
import dotenv from 'dotenv'
dotenv.config();
const PORT = 3000;


const app = express();

const ProductSchema = new mongoose.Schema({
    name: String,
    price: {
        type: String,
        required: true,
    },
}, { timestamps: true })

const Product = mongoose.model('Products', ProductSchema);

mongoose.connect(process.env.DB_CONNECT, () => {
    console.log('-----------------------------------------------------')
    console.log('DB Connected');
    console.log('-----------------------------------------------------')

    app.get('/', async (req, res) => {

        // Product.find((err, data) => {
        //     console.log(data);
        //     res.json(data);
        // })

        // const data = await Product.find({ price: '200' });
        try {
            // const data = await Product.findById('61c85ae4c38fbc85ed2de647');
            // const data = await Product.findOne({ name: 'orange' });

            // const newProduct = new Product({name: '1111', price: '400' });
            // const data = await newProduct.save();
            // const data = await Product.findByIdAndUpdate('61c85ae4c38fbc85ed2de647', { price: '150' });
            // const data = await Product.findByIdAndDelete('61c85ae4c38fbc85ed2de647');

            // const data = await Product.findById('61c85ed11b46f663c820acb3');
            // console.log(data.createdAt.toLocaleString());

            res.json(data);
        } catch (error) {
            res.send(error)
        }

    })

    app.listen(PORT, () => {
        console.log(`Server run on ${PORT}`);
    })
})

