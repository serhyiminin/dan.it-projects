import React from "react";
import {Switch, Route} from 'react-router-dom'
import HomePage from '../pages/HomePage'
import AddPostPage from '../pages/AddPostPage'

const AppRoutes = () => (
    <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/add-post" component={AddPostPage} />
    </Switch>
);


export default AppRoutes