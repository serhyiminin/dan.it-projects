import React from "react";
import styles from "./HeaderNav.module.scss"
import {Link, NavLink} from "react-router-dom";


const HeaderNav = () => {
    return(
        <nav>
            <ul>
                <li>
                    <NavLink exact to="/" activeClassName={styles.activeLink}>Home</NavLink>
                </li>
                <li>
                    <NavLink exact to="/add-post" activeClassName={styles.activeLink}>Add Post</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default HeaderNav