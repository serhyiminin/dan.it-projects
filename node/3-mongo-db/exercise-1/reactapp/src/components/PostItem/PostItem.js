import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styles from './PostItem.module.scss'
import PostForm from '../PostForm'
import { Button } from '@material-ui/core'

const PostItem = (props) => {
	const { id, title, text, setPosts } = props
	const [isEdit, setIsEdit] = useState(false)

	const deletePost = async () => {
		const data = await fetch(`http://localhost:8080/posts?id=${id}`, {
			method: 'DELETE'
		}).then(res => res.json());

		setPosts(data);
	}

	if (isEdit) {
		return (
			<div className={styles.root}>
				<PostForm title={title} text={text} submitCallback={() => setIsEdit(false)} />
			</div>
		)
	}

	return (
		<div className={styles.root}>
			<h3>{title}</h3>
			<p>{text}</p>

			<div className={styles.btnWrapper}>
				<Button color="primary" variant="contained" type="button" onClick={() => setIsEdit(true)}>Edit</Button>
				<Button onClick={deletePost} color="secondary" variant="contained" type="button">Delete</Button>
			</div>
		</div>
	)
}

PostItem.propTypes = {
	title: PropTypes.string,
	text: PropTypes.string,
	setPosts: PropTypes.func,
}
PostItem.defaultProps = {
	title: '',
	text: '',
	setPosts: () => {}
}

export default PostItem