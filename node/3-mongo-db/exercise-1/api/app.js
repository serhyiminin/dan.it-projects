import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import routes from "./routes/index.js";
import cors from "cors";
import bodyParser from "body-parser";
dotenv.config();

const port = 8080;
const app = express();

app.use(cors())
app.use(bodyParser.json())

mongoose.connect(process.env.DB_CONNECT, () => {
    console.log('-----------------------------------------------------');
    console.log('DB Connected');
    console.log('-----------------------------------------------------');

    routes(app);

    app.listen(port, () => {
        console.log(`Server opened at port ${port}`)
    })
});
